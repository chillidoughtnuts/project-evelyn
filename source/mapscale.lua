local mapscale = {scaleFactor = 1,dx = 0,dy = 0}

function mapscale.resizeMap(w,h)
	local mh, mw, mtw , mth = map.sti.height , map.sti.width, map.sti.tilewidth, map.sti.tileheight
	local mapRatio = (mh * mth)/(mw * mtw)
	if h/w >  mapRatio then
		mapscale.scaleFactor = (w - w%mtw) / (mw* mtw)
	else
		mapscale.scaleFactor = (h - h % mth) / (mh * mth)
	end
	--scaleFactor = math.floor(scaleFactor)
	mapscale.dy = math.floor((h-(mh * mth *mapscale.scaleFactor))/2)
	mapscale.dx = math.floor((w-(mw * mtw * mapscale.scaleFactor))/2)
	map.sti:resize(w,h)
end
function mapscale.scale()
	
	love.graphics.translate(mapscale.dx,mapscale.dy)
	love.graphics.scale(mapscale.scaleFactor)
	
	
end
return mapscale