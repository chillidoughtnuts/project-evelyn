--Boring Things
gKeyPressed = {}
gMousePressed = {}
function love.mousepressed(x,y,button)
	gMousePressed[button] = true
	gooi.pressed()
end
function love.mousereleased(x,y,button)
	gMousePressed[button] = nil
	gooi.released()
end

function love.keyreleased( key )
	gKeyPressed[key] = nil
end
function love.keypressed( key, unicode ) 
	gKeyPressed[key] = true 
	if (key == "escape") then os.exit(0) end
	if key == "f" then
		if love.window.getFullscreen() then
			love.window.setFullscreen(false)
		else
			love.window.setFullscreen(true)
		end
	end
end
local _newImage = love.graphics.newImage -- the old function
function love.graphics.newImage(...) -- new function. The ... is to forward any parameters to the old function
     local img = _newImage(...)
     img:setFilter('nearest', 'nearest')
     return img
end

local _getMousePosition = love.mouse.getPosition --the old function
function love.mouse.getPosition(...)
	local posx,posy = _getMousePosition(...)
	posx,posy = (posx-mapscale.dx)/mapscale.scaleFactor,(posy-mapscale.dy)/mapscale.scaleFactor
	return posx,posy
	
end

local _getDrawFunction = love.graphics.draw
function love.graphics.draw(...)
	for name,value in pairs(arg) do
		if name == "x" or name == "y" or name == "offsetx" or name == "offsety" then
		 arg[name] = math.floor(value)
		end
	end
	return _getDrawFunction(...)
end

function destroy(tablename)
	
	for q,e in pairs(tablename) do
		if type(e) == "table" then
			destroy(e)
		end
		tablename[q] = nil
			
	end
end