local gun = {
	tilesheetpath = "images/gun.png",
	tilewidth = 39,
	tileheight = 32,
	x = 0,
	y = 0,
	xspd = 1000,
	angle = 0,
	ox = 13,
	oy = 19,
	sprites = {},
	flip = 1,
	firingfrequency = 1/3,
	timer = {
		shoot = 0
	}
}

function gun.init()
	gun.tilesheet = love.graphics.newImage(gun.tilesheetpath)

	--Setting up quads from the gun tilesheet in table gun[].
	--ordered gun.sprites[x][y], x and y in tiles
	
	for x = 1, gun.tilesheet:getWidth()/gun.tilewidth do 
		gun.sprites[x] = {}
		for y = 1, gun.tilesheet:getHeight()/gun.tileheight do
			table.insert(
				gun.sprites[x],
				y,
				love.graphics.newQuad(
					(x-1)*gun.tilewidth,
					(y-1)*gun.tileheight,
					gun.tilewidth,
					gun.tileheight,
					gun.tilesheet:getWidth(),
					gun.tilesheet:getHeight()
				)
			)
		end
	end
	
	gun.currentsprite = gun.sprites[1][1]
end
local function shootanimation(dt)
	if gun.timer.shoot > gun.firingfrequency then
		gun.ox = 13
	else
		gun.ox = 13 - gun.timer.shoot*(1/gun.firingfrequency) * 5 + 4
	end
end

function gun.update(dt,px,py)
	local x,y = love.mouse.getPosition()
	local tempx,tempy = x-px,y-py
	local angle = math.atan2(tempy,tempx)
	if math.abs(angle) > math.pi/2 then
		angle = angle +math.pi
		gun.flip = -1
	else
		gun.flip = 1
	end
	gun.angle = angle 
	
	gun.x,gun.y = px,py
	
	
	shootanimation(dt)
	
	
	for timername, timervalue in pairs(gun.timer) do
		gun.timer[timername] = timervalue + dt --increment timer for gun
	end
end
function gun.draw()
	love.graphics.draw(
		gun.tilesheet,
		gun.currentsprite,
		gun.x,
		gun.y,
		gun.angle,
		gun.flip,
		1,
		gun.ox,
		gun.oy
	)
end
return gun