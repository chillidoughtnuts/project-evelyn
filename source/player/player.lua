
local player = {
		fizz = fizz.addDynamic("rect", 256,0,8,16),
		tilesheetpath = "images/player.png",
		jumptilesheetpath = "images/playerJump.png",
		sprites = {},
		speed = 200, --acheivable horizontal speed
		jump = 300, --jump height
		flip = -1,
		terminalv = 750,
		grounded = false,
		still = 0,
		x = 0,
		y = 0,
		timer = {
			animatedsprite = 0
		},
		gravity = 468,
		override = {
		}
	}
local gun = require "player/gun"
function player.init()

	player.fizz.gravity = player.gravity
	player.tilesheet = love.graphics.newImage(player.tilesheetpath)
	for x = 1, player.tilesheet:getWidth()/32 do
		player.sprites[x] = {}
		for y = 1, player.tilesheet:getHeight()/32 do
			table.insert(
				player.sprites[x],
				y,
				love.graphics.newQuad(
					(x-1)*32,
					(y-1)*32,
					32,
					32,
					player.tilesheet:getWidth(),
					player.tilesheet:getHeight()
				)
			)
		end
	end
	player.currentsprite = player.sprites[1][1]
	gun.init()
	
	
	player.jumpAnimation  = katsudo.new(player.jumptilesheetpath, 20, 20, 3, 0.02)
end
local animatedsprite = function(state)

	if state ~= "still" and player.still == 0 then
		if player.timer.animatedsprite > 15/player.speed then
			player.timer.animatedsprite = 0
			if state == "forward" then
				return player.currentsprite == player.sprites[2][1] and player.sprites[3][1] or player.sprites[2][1]
			elseif state == "backward"  then
				return player.currentsprite == player.sprites[4][1] and player.sprites[5][1] or player.sprites[4][1]
			end
		end
	else
		return player.sprites[1][1] --return still sprite
	end
	return player.currentsprite
end
local function animationupdate()
	local xspd,yspd = fizz.getVelocity(player.fizz) --get velocity of player
	local sx, sy = fizz.getDisplacement(player.fizz)
	
	if xspd < 0 then
		player.currentsprite =  player.flip < 0  and animatedsprite("forward") or animatedsprite("backward")
	elseif xspd > 0 then
		player.currentsprite =  player.flip > 0 and animatedsprite("forward") or animatedsprite("backward")
	else
		player.currentsprite = animatedsprite("still")
	end
end

function player.draw()
	local xspd,yspd = fizz.getVelocity(player.fizz)
	if player.grounded then --or ((player.jump + yspd) < player.jump/10 or  xspd == 0 )then
		love.graphics.draw(player.tilesheet,player.currentsprite,player.x,player.y,0,player.flip,1,16,16)
	else
		local flip = xspd > 0 and -1 or 1
		
		player.jumpAnimation:draw(player.x,player.y,0,flip,1,10,10)
	end
	gun.draw()
end

local function shoot()
	local xpos,ypos = fizz.getPosition(player.fizz) 
	if gun.timer.shoot > 1/5 then
		local angle = gun.flip > 0 and gun.angle or gun.angle - math.pi
		entities:create("projectile.defaultbullet",{angle = angle,x = xpos,y = ypos, speed = 750})
		gun.timer.shoot = 0
	end
end


function player.update(dt)
	--get information from fizz shape
	animationupdate()
	local xpos,ypos = fizz.getPosition(player.fizz) --get position of player
	local xspd,yspd = fizz.getVelocity(player.fizz) --get velocity of player
	local sx, sy = fizz.getDisplacement(player.fizz) --get seperation vector for collision (basically direction pushed caused by collisions)
	
	for r,t in pairs(player.override) do
		if r == "xspd" then
			xspd = t
			player.override[r] = nil
		elseif r == "yspd" then
			xspd = t
			player.override[r] = nil
		end
	end
	if yspd > player.terminalv then
		yspd = player.terminalv
	end
	--check if touching the ground and then storing in player.grounded
	if sy < 0 then
		player.grounded = true -- is touching the ground
	else
		player.grounded = false --is not touching the ground
	end
	
	
	if gMousePressed[1] then
		shoot()
	end
	
	
	--horizontal movement controls
	if gKeyPressed[keybindings.moveleft] and not gKeyPressed[keybindings.moveright] then --If pressing left and not right then
		xspd = -player.speed --set horizontal velocity leftwards
	elseif gKeyPressed[keybindings.moveright] and not gKeyPressed[keybindings.moveleft] then --else if pressing right and not left then
		xspd = player.speed --set horizontal velocity rightward
	elseif player.grounded or math.abs(xspd) <= 0.2 then --else if touching the ground or magnitude of horizontal speed is small then
		xspd = 0 --stop all horizontal movement
	else --If none of the above are true (neither left nor right are pressed and not touching the ground)
	
		--for horizontal deceleration in the air
		if xspd > 0 then --if horizontal velocity is rightwards (is positive)
			xspd = xspd - 100*dt --decrease horizontal velocity (increase towards left)
		elseif xspd < 0 then --if horizontal velocity is leftwards (is negative)
			xspd = xspd + 100*dt --decrease horizontal velocity (increase towards right)
		end
		
	end
	
	
	--jumping mechanics
	if player.grounded then --if touching the ground then
		if gKeyPressed[keybindings.jump] then--If also pressing up then 
			yspd = -player.jump --set vertical velocity upwards
		end
	end
	
	
	
	--WRAPPING PLAYER--
	--PROBABLY TEMPORARY
	local xdiff,ydiff,mapw,maph = map.isOffMap(xpos,ypos)
	if ydiff and ydiff < -32 then--if player y pos is too far below the map then
		fizz.setPosition(player.fizz,xpos,0)--set player y pos to above map
	end
	if xdiff and xdiff > 16 then -- if player x pos is too far to the right of map then
		fizz.setPosition(player.fizz,0,ypos) -- set player x position to the left of the map
	elseif xdiff and xdiff < -16 then --else if player x pos is too far to the left of map then
		fizz.setPosition(player.fizz,mapw,ypos) --set player x position to right of map
	end
	--END OF WRAPPING
	
	fizz.setVelocity(player.fizz, xspd,yspd) --send velocity information to fizz shape
	
	
	gun.update(dt,xpos,ypos)--update gun position
	player.flip = gun.flip
	
	for timername, timervalue in pairs(player.timer) do
		player.timer[timername] = timervalue + dt --increment timer for animation of player
	end
	player.x,player.y = fizz.getPosition(player.fizz)
end
return player