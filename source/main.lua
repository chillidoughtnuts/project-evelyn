require "things"
require "library/deepcopy"
fizz = require "library/fizzx/fizz"
mapscale = require "mapscale"
map = require "map"
player = require "player"
entities = require "scripts/entities"
mapscripts = require "scripts/maps"
keybindings = require "keybindings"
dialogue = require "scripts/dialogue"
katsudo = require "library/katsudo"
require "library/gooi"
--require "library/lovedebug"
--require "library/lovedebug"

local collisionTiles = {}

gamepause = false

function love.load()	
	
	map.reload()
	player.init()
	love.graphics.setBackgroundColor(50,50,50)
end




function love.resize(w,h)
	mapscripts.resize(w,h)
end


accumtime = 0
drawPS = 0

timeshift = 1
function love.update(dt)
	accumtime = accumtime + dt
	drawPS = 1/dt
	
	local interval = 0.0005
	
	timeshift = 1
	
	
	if gKeyPressed[keybindings.s1] then
		timeshift = timeshift/2
	end
	if gKeyPressed[keybindings.s2] then
		timeshift = timeshift/2
	end
	if gKeyPressed[keybindings.s3] then
		timeshift = timeshift/2
	end
	if gKeyPressed[keybindings.s4] then
		timeshift = timeshift*2
	end
	
	if gKeyPressed.t then
		timeshift = 0
		--gamepause = true
	else
		--gamepause = false
	end
	while accumtime >= interval do
		mapscripts.update(interval*timeshift)
		katsudo:update(interval*timeshift)
		accumtime = accumtime - interval
	end
	
	if map.reloadmap then	
		map.reload()
	end
	
	
end

function love.draw()	
	mapscripts.draw()
end



