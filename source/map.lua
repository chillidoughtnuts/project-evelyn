local map = {
	mappaths = require("scripts/maps/mappaths"),
	reloadmap = false,
	--targetmappath = "maps/Testing/FirstLevel.lua",
	enterpath = ""
}
local sti = require "library/sti"
function map.reloadFitz()
	map.clearFitz()
	fizzTiles = map.fizzLoadMap()
	fizz.setGravity(0,1)
end

function map.clearmap()

	if map.sti then
		map.sti = nil
	end
	map.sti = {}
end
function map.loadmap(mappath)
	map.clearmap()
	map.sti = sti.new(mappath)
end	
function map.update(dt)
	map.sti:update(dt)
end
function map.reload(targetscript)
	map.reloadmap = true
	local mapGo = true
	local script = targetscript
	if targetscript or not map.targetmappath then
		if not targetscript then
			script = "titlescreen"
		end
		mapGo = false
	end
	
	mapscripts:destroy()	
	entities:destroy()
	if mapGo then
		map.loadmap(map.targetmappath)
		map.reloadFitz()
	else
		map.clearmap()
		map.clearFitz()
	end
	if mapGo then
		entities:load()
		entities:init()
	end
	
	mapscripts.load(script)
	mapscripts.init()
	
	map.enterpath = ""
	map.reloadmap = false
	
	
	love.resize(love.graphics.getWidth(),love.graphics.getHeight())
end

function map.searchMapPaths(targetexit)
	assert(type(targetexit) == "string", "Targeted exit map variable is not string")
	if targetexit == "none" then
		return nil
	end
			
	local dlist = map.mappaths
	
	for currentexit in string.gmatch(targetexit,"%w+") do
		for q,e in pairs(dlist) do
			if q == currentexit then
				if type(e) == "table" then
					dlist = e
				else
					return e
				end
				
			end
		end
	end
	
	
	error("Could not find targeted exit map")
	
end
function map.getSize()
	local tempi = -1
	for i=1,#map.sti.layers do
		if map.sti.layers[i].data then
			tempi = i
			break
		end
	end
	
	local mapw,maph
	if tempi == -1 then 
		mapw = love.graphics.getWidth()
		maph = love.graphics.getHeight()
	else
		mapw = map.sti.layers[tempi].width*32 --get width of map in pixels
		maph = map.sti.layers[tempi].height*32 --get height of map in pixels
	end
	
	return mapw,maph
end
function map.isOffMap(xpos,ypos)
	
	local mapw,maph = map.getSize()
	local xoff,yoff = false,false
	if ypos > maph then--if y pos is too far below the map then
		yoff = ypos - maph
	elseif ypos < 0 then --else if y pos is too far above the map then
		yoff = maph
	end
	if xpos > mapw then -- if x pos is too far to the right of map then
		xoff = xpos - mapw
	elseif 0 > xpos then --else if  x pos is too far to the left of map then
		xoff = xpos
	end
	--return false,false
	return xoff,yoff,mapw,maph
end

function map.clearFitz()
	local temp = 0
	if fizzTiles then
	for y,xField in pairs(fizzTiles) do
		for x,shapelist in pairs(xField) do
			for shape,value in pairs(shapelist) do
				if shape == "rectangle" then
					fizz.removeShape(value)
				elseif shape == "polygon" then
					for i=1,#value do
						fizz.removeShape(value[i])
					end
				end
			end
		end
	end
	end
end
function map.checkIfCollidable(propertiesList)
	for propertyName, propertyState in pairs(propertiesList) do
		if propertyName == "Collidable" and propertyState then
			return true
		end
	end
	return false
end
function map.checkIfLayerCollidable(layerList)
	for layerField, fieldList in pairs(layerList) do
		if layerField == "properties" then
			return map.checkIfCollidable(fieldList)
		end
	end
	return false
end
function map.getCollidableLayers()
	local tileList = {}
	local layerList  = {}
	for layerName, field in pairs(map.sti.layers) do
		if map.checkIfLayerCollidable(field) then
			layerList[layerName] = field.data
		end
	end
	return layerList
end
function map.getCollidablePos()
	local collidableLayers = map.getCollidableLayers()
	local tempcount = 0
	local tempArray = {}
	for layer, yField in pairs(collidableLayers) do
		for y, xField in pairs(yField) do
			local tempXList = {}
			for x, field in pairs(xField) do 
				tempXList[x] = field
			end
			tempArray[y] = tempXList
		end
	end
	return tempArray
end
function map.checkTileType(field,up,down,left,right)
	local polygonList = {}
	for element, value in pairs(field) do
		if element == "properties" then
			if type(value) == "table" then
				
				for propertyName, propertyValue in pairs(value) do
					
					
					if propertyName == "polygon" then
						for number in propertyValue:gmatch("[%d%-]+") do
							table.insert(polygonList,number)
						end
					end
				end
			end
		end
	end
	if #polygonList > 0 then
		local function createBlock(xpos,ypos) 
			local tempList = {}
			tempList.polygon = {}
			for i = 1, #polygonList, 4 do
				local x2,y2 = tonumber(polygonList[i+2]),tonumber( polygonList[i+3])
				local x,y = tonumber(polygonList[i]),tonumber(polygonList[i+1])
				if up and y-y2 == 0 and x > x2 and y < 4 then
				elseif down and y-y2 == 0 and x < x2 and y > 28  then
				elseif left and x-x2 == 0 and y < y2 and x < 4  then
				elseif right and x-x2 == 0 and y > y2 and x > 28  then
				else
					table.insert(tempList.polygon,fizz.addStatic("line",xpos+x-32,ypos+y-32,xpos+x2-32,ypos+y2-32))
				end
			end
			return tempList
		end
		return createBlock
	end
	
	return nil
end
function map.fizzLoadMap()
	local colTilePos = map.getCollidablePos()
	local tilesFizz = {}
	if colTilePos then
		for y,xField in pairs(colTilePos) do
			local tempXList = {}
			for x, field in pairs(xField) do
				local w,h = field.width,field.height
				local posx,posy = (x)*w, (y)*h
				local up,down,left,right = false,false,false,false
				if colTilePos[y+1] then
					if colTilePos[y+1][x]  then
						down = true
					end
				end
				if colTilePos[y-1] then
					if colTilePos[y-1][x]  then
						up = true
					end
				end
				if colTilePos[y][x+1] then
					right = true
				end
				if colTilePos[y][x-1] then
					left = true
				end
				local tileType = map.checkTileType(field,up,down,left,right)
				if tileType then
					
					table.insert(tempXList,x,tileType(posx,posy))
				else
					
					table.insert(tempXList,x,{rectangle = fizz.addStatic("rect",posx-(w/2),posy-(h/2),16,16)})
					if up then
						tempXList[x].rectangle.bottom = true
					end
					if down then
						tempXList[x].rectangle.top = true
					end
					if left then
						tempXList[x].rectangle.left = true
					end
					if right then
						tempXList[x].rectangle.right = true
					end
				end
			end
			table.insert(tilesFizz,y,tempXList)
		end
	end
	return tilesFizz
end

return map