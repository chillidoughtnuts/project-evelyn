local blackcubeofdoom = {
		name = "blackcubeofdoom",
		x = 0,
		y = 0,
		dx = 0,
		dy = 0,
		angle = 0,
		spritepath = "images/blackcubeofdoom.png",
		speed = 200,
		ox = 16,
		oy = 16,
		gravity = 0
	}
function blackcubeofdoom:init()

	self.x = tonumber(self.x)
	self.y = tonumber(self.y)
	self.dx = tonumber(self.dx)
	self.dy = tonumber(self.dy)
	self.angle = tonumber(self.angle)
	self.speed = tonumber(self.speed)
	self.ox = tonumber(self.ox)
	self.oy = tonumber(self.oy)
	self.gravity = tonumber(self.gravity)
	
	self.sprite = love.graphics.newImage(self.spritepath)
	self.ox = self.sprite:getWidth()/2
	self.oy = self.sprite:getHeight()/2
	self.fizz = fizz.addDynamic("rect", self.x,self.y,self.ox,self.oy)
	self.fizz.gravity = self.gravity
end

function blackcubeofdoom:draw()
	if self.sprite then
		love.graphics.draw(
			self.sprite,
			self.x,
			self.y,
			self.angle,
			1,
			1,
			self.ox,
			self.oy
		)
	else
		love.graphics.print("blackcubeofdoom",self.x,self.y)
	end
end

function blackcubeofdoom:update(dt)
	local xpos,ypos = fizz.getPosition(self.fizz) 
	self.x = xpos
	self.y = ypos
	
	
	
end

return blackcubeofdoom