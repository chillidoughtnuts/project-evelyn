local filers = {
		name = "filers",
		x = 0,
		y = 0,
		dx = 0,
		dy = 0,
		angle = 0,
		spritepath = "images/blackcubeofdoom.png",
		speed = 100,
		ox = 16,
		oy = 16,
		gravity = 0,
		sleep = true,
		activated = false,
		range = 192,
		shootrange = 64,
		loaded = true,
		bulletspeed = -1
	}
function filers:init()
	self.x = tonumber(self.x)
	self.y = tonumber(self.y)
	self.dx = tonumber(self.dx)
	self.dy = tonumber(self.dy)
	self.angle = tonumber(self.angle)
	self.range = tonumber(self.range)
	self.shootrange = tonumber(self.shootrange)
	self.speed = tonumber(self.speed)
	self.ox = tonumber(self.ox)
	self.oy = tonumber(self.oy)
	self.gravity = tonumber(self.gravity)
	
	self.sprite = love.graphics.newImage(self.spritepath)
	self.ox = self.sprite:getWidth()/2
	self.oy = self.sprite:getHeight()/2
	self.fizz = fizz.addDynamic("rect", self.x,self.y,self.ox,self.oy)
	self.fizz.right = true
	self.fizz.left = true
	self.fizz.bottom = true
	self.fizz.top = true
	self.fizz.gravity = self.gravity
	
	
end

function filers:draw()
	if self.sprite then
		love.graphics.draw(
			self.sprite,
			self.x,
			self.y,
			self.angle,
			1,
			1,
			self.ox,
			self.oy
		)
	else
		love.graphics.print("filers",self.x,self.y)
	end
end

function filers:update(dt)
	local xpos,ypos = fizz.getPosition(self.fizz) 
	self.x = xpos
	self.y = ypos
	
	
			
	if self.sleep and math.abs(self.x - player.x) < self.range then
		self.sleep = false
	end
	if not self.sleep and not self.activated then
		self.activated = true
		if self.x > player.x then
			fizz.setVelocity(self.fizz,-self.speed,0)
			
		else
			fizz.setVelocity(self.fizz,self.speed,0)
		end
		
		
	end
	if self.activated and self.loaded and math.abs(self.x - player.x) < self.shootrange then
		self.loaded = false
		
		local tempangle = math.atan2(-self.y+player.y,-self.x+player.x)
		local yoffset = math.sin(tempangle)*self.oy*2 + self.y
		local xoffset = math.cos(tempangle)*self.ox*2 + self.x
		if self.bulletspeed  == -1 then
			entities:create("projectile.defaultbullet",{angle = tempangle,x = xoffset,y = yoffset})
		else 
			entities:create("projectile.defaultbullet",{angle = tempangle,x = xoffset,y = yoffset,speed = self.bulletspeed})
		end
	end
end

return filers