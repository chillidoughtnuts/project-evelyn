local sums = {
		name = "sums",
		x = 0,
		y = 0,
		dx = 0,
		dy = 0,
		angle = 0,
		stepdistance = 96,
		originx = 0,
		shooting = false,
		shootinterval = 1,
		tilesheetpath = "images/sums.png",
		sprites = {},
		speed = -100,
		ox = 16,
		oy = 16,
		gravity = 0,
		bulletspeed = -1,
		state = 0,
		shootorient = "vert",
		timer = {
			shooting = 0
		}
	}
function sums:init()
	
	self.x = tonumber(self.x)
	self.y = tonumber(self.y)
	self.dx = tonumber(self.dx)
	self.dy = tonumber(self.dy)
	self.angle = tonumber(self.angle)
	self.stepdistance = tonumber(self.stepdistance)
	self.originx = tonumber(self.originx)
	self.shootinterval = tonumber(self.shootinterval)
	self.speed = tonumber(self.speed)
	self.ox = tonumber(self.ox)
	self.oy = tonumber(self.oy)
	self.gravity = tonumber(self.gravity)
	self.timer.shooting = tonumber(self.timer.shooting)
	
	self.tilesheet = love.graphics.newImage(self.tilesheetpath)
	for x = 1, self.tilesheet:getWidth()/31 do
		self.sprites[x] = {}
		for y = 1, self.tilesheet:getHeight()/25 do
			table.insert(
				self.sprites[x],
				y,
				love.graphics.newQuad(
					(x-1)*31,
					(y-1)*25,
					31,
					25,
					self.tilesheet:getWidth(),
					self.tilesheet:getHeight()
				)
			)
		end
	end
	self.currentsprite = self.sprites[1][1]
	
	
	self.ox = 31/2
	self.oy = 25/2
	
	self.fizz = fizz.addKinematic("rect", self.x,self.y,self.ox,self.oy)
	--[[
	self.fizz.top = true
	self.fizz.bottom = true
	self.fizz.left = true
	self.fizz.right = true]]
	self.fizz.gravity = self.gravity
	self.originx = self.x
	
	
	
	
end

function sums:updateanimation()
	if self.state == 0 then
		self.currentsprite = self.sprites[1][1]
	elseif self.state == 1 then
		self.currentsprite = self.sprites[2][1]
	else
		self.currentsprite = self.sprites[3][1]
	end
end

function sums:draw()
	if self.currentsprite then
		love.graphics.draw(
			self.tilesheet,
			self.currentsprite,
			self.x,
			self.y,
			self.angle,
			1,
			1,
			self.ox,
			self.oy
		)
	else
		love.graphics.print("sums",self.x,self.y)
	end
	love.graphics.print(math.random(),0,100)
end

function sums:update(dt)
	local xpos,ypos = fizz.getPosition(self.fizz) 
	self.x = xpos
	self.y = ypos
	if not self.shooting and self.timer.shooting > self.shootinterval then
		fizz.setVelocity(self.fizz,self.speed,0)
		if math.abs(self.x-self.originx) > self.stepdistance then
			self.shooting = true
			self.timer.shooting = 0
			local randomnum = math.random()
			if randomnum > 0.5 then
				self.shootorient = "horz"
			else
				self.shootorient = "vert"
			end
		end
		self.state = 0
	else
		fizz.setVelocity(self.fizz,0,0)	
		
		if  self.timer.shooting*5 > self.shootinterval and self.shooting then
			self.shooting = false
			self.originx = self.x
			local xoffset
			local yoffset
			local tempangle
			if self.shootorient == "vert" then
				tempangle = self.angle 
			else
				tempangle = self.angle + math.pi/2
			end
			
			if self.shootorient == "vert" then
				yoffset = 0
				xoffset = self.ox 
			else
				xoffset = 0
				yoffset = self.oy
			end
			if self.bulletspeed == -1 then
					entities:create("projectile.defaultbullet",{angle =tempangle,x = self.x + xoffset,y = self.y + yoffset})
					entities:create("projectile.defaultbullet",{angle =tempangle-math.pi,x = self.x - xoffset,y = self.y - yoffset})
			else
				entities:create("projectile.defaultbullet",{angle = tempangle,x = self.x + xoffset,y = self.y + yoffset,speed = self.bulletspeed})
				entities:create("projectile.defaultbullet",{angle = tempangle-math.pi,x = self.x - xoffset,y = self.y - yoffset,speed = self.bulletspeed})
				
			end
			
		elseif self.shooting then
			if self.shootorient == "vert" then
				self.state = 2
			else
				self.state = 1
			end
		end
		
	end
	
	self.timer.shooting = self.timer.shooting + dt
	self:updateanimation()
end

return sums