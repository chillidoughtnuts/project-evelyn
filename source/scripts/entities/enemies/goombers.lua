local goombers = {
		name = "goombers",
		x = 0,
		y = 0,
		dx = 0,
		dy = 0,
		angle = 0,
		spritepath = "images/goomber.png",
		speed = 100,
		ox = 16,
		oy = 16,
		gravity = 5000,
		direction = -1,
		playeron = false
	}
function goombers:init()
	self.x = tonumber(self.x)
	self.y = tonumber(self.y)
	self.dx = tonumber(self.dx)
	self.dy = tonumber(self.dy)
	self.angle = tonumber(self.angle)
	self.speed = tonumber(self.speed)
	self.ox = tonumber(self.ox)
	self.oy = tonumber(self.oy)
	self.gravity = tonumber(self.gravity)
	self.direction = tonumber(self.direction)
	
	self.sprite = love.graphics.newImage(self.spritepath)
	self.ox = self.sprite:getWidth()/2
	self.oy = self.sprite:getHeight()/2
	self.fizz = fizz.addDynamic("rect", self.x,self.y,self.ox,self.oy)
	self.fizz.gravity = self.gravity
	
	
end

function goombers:draw()
	if self.sprite then
		love.graphics.draw(
			self.sprite,
			self.x,
			self.y,
			self.angle,
			-self.direction,
			1,
			self.ox,
			self.oy
		)
	else
		love.graphics.print("goombers",self.x,self.y)
	end
end

function goombers:update(dt)
	self.x,self.y = fizz.getPosition(self.fizz)
	self.dx,self.dy = fizz.getVelocity(self.fizz)
	local sx,sy = fizz.getDisplacement(self.fizz)
	
	if sx > 0 then
		self.direction = 1
	elseif sx < 0 then
		self.direction = -1
	else
		local speeddif = self.speed*self.direction - self.dx
		if  math.abs(speeddif) > self.speed/100 then
			if speeddif > 0 then
				self.direction = 1
			else
				self.direction = -1
			end
		end
	end
	
	
	local nx, ny, pen = fizz.isCollide(self.fizz,player.fizz)
	
	
	if (nx or ny or pen) and ny ~= 0 then
		local pvx,pvy = fizz.getVelocity(player.fizz)
		if not self.playeron then
			self.playeron = true
			player.still = player.still + 1
		end
		if (gKeyPressed.left or gKeyPressed.right) then
			self.playeron = false
			player.still = player.still -1
		end
		
		
		fizz.setVelocity(player.fizz,self.speed*self.direction+pvx,pvy)
	else
		if self.playeron then
			self.playeron = false
			player.still = player.still -1
		end
	end
	
	fizz.setVelocity(self.fizz,self.speed*self.direction,self.dy)
end

return goombers