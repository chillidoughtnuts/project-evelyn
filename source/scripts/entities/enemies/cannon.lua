local cannon = {
		name = "cannon",
		x = 0,
		y = 0,
		dx = 0,
		dy = 0,
		spritepath = "images/cannon.png",
		speed = 200, -- in pixels(relative) per second
		acceleration = 1000, -- in pixels per second squared
		ox = 16,
		oy = 16,
		gravity = 0,
		bulletgravity = 500,
		bulletdecel = 100,
		pathString = "",
		angleString = "",
		shotTargetString = "",
		paths = {{x = 0, y = 0}},
		angles = {0,0},
		shotTargets = {
			{
				x = 0,
				y = 0
			}
		},
		target = 1,
		switch = true,
		shootinterval = 2,
		timer = {
			getpoint = 10,
			shoot = 48
		},
		testprint = true
	}
function cannon:init()

	self.x = tonumber(self.x)
	self.y = tonumber(self.y)
	self.dx = tonumber(self.dx)
	self.dy = tonumber(self.dy)
	self.speed = tonumber(self.speed)
	self.ox = tonumber(self.ox)
	self.oy = tonumber(self.oy)
	self.gravity = tonumber(self.gravity)
	self.acceleration = tonumber(self.acceleration)
	self.target = tonumber(self.target)
	
	self.bulletgravity = tonumber(self.bulletgravity)
	self.bulletdecel = tonumber(self.bulletdecel)
	
	self.sprite = love.graphics.newImage(self.spritepath)
	self.ox = self.sprite:getWidth()/2
	self.oy = self.sprite:getHeight()/2
	self.fizz = fizz.addKinematic("rect", self.x,self.y,self.ox,self.oy)
	self.fizz.gravity = self.gravity

	
	if self.pathString ~= "" then
		local numArray = {}
		local count1,count2 = 1,1
		for number in self.pathString:gmatch("[%d%-]+") do
			if not numArray[count1] then
				numArray[count1] = {}
			end
			local key = "none"
			if count2 == 1 then
				key = "x"
			else
				key = "y"
			end
			numArray[count1][key] = tonumber(number)
			count2 = count2 + 1
			if count2 > 2 then
				count1 = count1 + 1
				count2 = 1
			end
		end
		self.paths = numArray
	end
	
	if self.angleString ~= "" then
		local numArray = {}
		local count1,count2 = 1,1
		for number in self.angleString:gmatch("[%d%-]+") do
			table.insert(numArray,tonumber(number)*math.pi/180)
		end
		self.angles = numArray
	else
		while #self.angles < #self.paths do
			table.insert(self.angles,0)

		end
	end
	
	if self.shotTargetString ~= "" then
		local numArray = {}
		local count1,count2 = 1,1
		for number in self.shotTargetString:gmatch("[%d%-]+") do
			if not numArray[count1] then
				numArray[count1] = {}
			end
			local key = "none"
			if count2 == 1 then
				key = "x"
			else
				key = "y"
			end
			numArray[count1][key] = tonumber(number)
			count2 = count2 + 1
			if count2 > 2 then
				count1 = count1 + 1
				count2 = 1
			end
		end
		self.shotTargets = numArray
	else
		while #self.shotTargets < #self.paths do
			table.insert(self.shotTargets, {x = 0, y = 0})
		end
	end
end

function cannon:draw()
	if self.sprite then
		love.graphics.draw(
			self.sprite,
			self.x,
			self.y,
			self.angles[self.target]-math.pi,
			1,
			1,
			self.ox,
			self.oy
		)
	else
		love.graphics.print("cannon",self.x,self.y)
	end
	
end

function cannon:update(dt)
	local xpos,ypos = fizz.getPosition(self.fizz) 
	local xspd,yspd = fizz.getVelocity(self.fizz)
	self.x = xpos
	self.y = ypos
	
	
		local target = self.paths[self.target]
		local dx,dy = target.x - self.x, target.y - self.y
		local angle = math.atan2(dy,dx)
		local distance = (dx^2+dy^2)^0.5
		
		local speed = (xspd^2 + yspd^2)^0.5
		local flip = 1
		if math.abs(distance) <= math.abs((speed^2)/self.acceleration) then
			flip = -1
		end
		xspd = xspd + math.cos(angle)*self.acceleration*dt*flip
		yspd = yspd + math.sin(angle)*self.acceleration*dt*flip
		
		
		if speed > self.speed then
			
			xspd = math.cos(angle)*self.speed
			yspd = math.sin(angle)*self.speed
		end
		
		if (distance <= 0.2)then
			if self.switch then
				fizz.setPosition(self.fizz,
					self.paths[self.target].x,
					self.paths[self.target].y
				)
				if self.target == #self.paths then
					self.target = 1
				else
					self.target = self.target + 1
				end
				xspd,yspd = 0,0
				
			end
			
			self.timer.getpoint = 0
			self.switch = false
		else
			if self.timer.getpoint >  100/self.speed then
				self.switch = true
			end
		end
		fizz.setVelocity(self.fizz,xspd,yspd)
	
	
	
	if self.timer.shoot > self.shootinterval then
		local yoffset = math.sin(self.angles[self.target])*self.oy*2 + self.y
		local xoffset = math.cos(self.angles[self.target])*self.ox*2 + self.x
		
		local targetangle = self.angles[self.target]		
		
		local xdiff = (xoffset - self.shotTargets[self.target].x)
		local ydiff = (yoffset - self.shotTargets[self.target].y)
		
		if math.abs(targetangle-math.pi/2)%math.pi == 0 then
			if xdiff > 0 then
				targetangle = math.pi
			else
				targetangle = 0
			end
		else
			if ydiff > 0 then
				targetangle = -math.pi/2
			else
				targetangle = math.pi/2
			end
		end
		local lasttarget
		if self.paths[self.target-1] then
			lasttarget = self.paths[self.target-1] 
		else
			local temppathtarget = #self.paths
			lasttarget = self.paths[temppathtarget]
		end
		local nowtarget = self.paths[self.target]
		
		local first,second = (lasttarget.y-self.y),(lasttarget.y-nowtarget.y)
		local tempratiox
		if first == 0 or second == 0 then
			tempratiox = 0
		else
			tempratiox = math.abs(first/second)
		end
		first,second = (lasttarget.x-self.x),(lasttarget.x-nowtarget.x)
		
		local tempratioy
		if first == 0 or second == 0 then
			tempratioy = 0
		else
			tempratioy = math.abs(first/second)
		end
		
		
		if self.shotTargets[self.target-1] then
			lasttarget = self.shotTargets[self.target-1] 
		else
			local temppathtarget = #self.paths
			lasttarget = self.shotTargets[temppathtarget]
		end
		local shottargetx = ((self.shotTargets[self.target].x - lasttarget.x ) * tempratiox) + lasttarget.x
		local shottargety = ((self.shotTargets[self.target].y - lasttarget.y ) * tempratioy) + lasttarget.y
		entities:create(
			"projectile.cannonbullet",
			{
				angle = self.angles[self.target],
				x = xoffset,
				y = yoffset,
				starting = {
					x = xoffset,
					y = yoffset
				},
				target = {
					x = shottargetx,
					y = shottargety
				},
				gravity = self.bulletgravity,
				decel = self.bulletdecel
			}
		)
		self.timer.shoot = 0
	end
	
	self.timer.shoot = self.timer.shoot + dt
	self.timer.getpoint = self.timer.getpoint + dt
	
	
end

return cannon