local calmshell = {
		name = "calmshell",
		x = 0,
		y = 0,
		dx = 0,
		dy = 0,
		angle = 0,
		tilesheetpath = "images/clamshell.png",
		speed = 100,
		ox = 16,
		oy = 16,
		gravity = 0,
		direction = -1,
		bulletspeed = 100,
		sprites = {},
		playeron = false,
		clamclosed = true,
		bulletshot = true,
		durationtarget = 1,
		durationShellString = "",
		durationShootingString = "",
		duration = {
			shell = {
				5,3
			},
			shooting = {
				0.2,0.5
			}
		},
		timer = {
			shell = 10
		}
	}
	
function calmshell:updateanimation()
	if self.clamclosed then
		self.currentsprite = self.sprites[1][1]
	else
		self.currentsprite = self.sprites[2][1]
	end
end

function calmshell:init()
	self.x = tonumber(self.x)
	self.y = tonumber(self.y)
	self.dx = tonumber(self.dx)
	self.dy = tonumber(self.dy)
	self.angle = tonumber(self.angle)
	self.speed = tonumber(self.speed)
	self.ox = tonumber(self.ox)
	self.oy = tonumber(self.oy)
	self.gravity = tonumber(self.gravity)
	self.direction = tonumber(self.direction)
	
	
	self.tilesheet = love.graphics.newImage(self.tilesheetpath)
	for x = 1, self.tilesheet:getWidth()/33 do
		self.sprites[x] = {}
		for y = 1, self.tilesheet:getHeight()/26 do
			table.insert(
				self.sprites[x],
				y,
				love.graphics.newQuad(
					(x-1)*33,
					(y-1)*26,
					33,
					26,
					self.tilesheet:getWidth(),
					self.tilesheet:getHeight()
				)
			)
		end
	end
	
	self.currentsprite = self.sprites[1][1]
	self.ox = 33/2 --self.sprites[1][1]:getWidth()/2
	self.oy = 26/2 --self.sprites[1][1]:getHeight()/2
	
	if self.durationShellString ~= "" then
		local numArray = {}
		for number in self.durationShellString:gmatch("[%d%-]+") do
			table.insert(numArray,tonumber(number))
		end
		self.duration.shell = numArray
	end
	
	if self.durationShootingString ~= "" then
		local numArray = {}
		for number in self.durationShootingString:gmatch("[%d%-]+") do
			table.insert(numArray,tonumber(number))
		end
		self.duration.shooting = numArray
	
	end
	while #self.duration.shooting < #self.duration.shell do
		table.insert(self.duration.shooting,1)

	end
	
	self.fizz = fizz.addKinematic("rect", self.x,self.y,self.ox,self.oy)
	self.fizz.gravity = self.gravity
	
end



function calmshell:draw()
	if self.currentsprite then
		love.graphics.draw(
			self.tilesheet,
			self.currentsprite,
			self.x,
			self.y,
			self.angle,
			-self.direction,
			1,
			self.ox,
			self.oy
		)
	else
		love.graphics.print("calmshell",self.x,self.y)
	end
end

function calmshell:update(dt)
	self.x,self.y = fizz.getPosition(self.fizz)
	self.dx,self.dy = fizz.getVelocity(self.fizz)
	local sx,sy = fizz.getDisplacement(self.fizz)
	
	local diffx,diffy = player.x-self.x,player.y-self.y
	
	local angleTP = math.atan2(diffx,diffy)
	--self.angle = angleTP
	if angleTP > 0 then
		self.direction = 1
	else
		self.direction = -1
	end
	
	
	
	--[[turning around when hit something
	if sx > 0 then
		self.direction = 1
	elseif sx < 0 then
		self.direction = -1
	else
		local speeddif = self.speed*self.direction - self.dx
		if  math.abs(speeddif) > self.speed/100 then
			if speeddif > 0 then
				self.direction = 1
			else
				self.direction = -1
			end
		end
	end
	--]]
	
	
	--[[when collide with player
	local nx, ny, pen = fizz.isCollide(self.fizz,player.fizz)
	if (nx or ny or pen) and nx == 0 then
		local pvx,pvy = fizz.getVelocity(player.fizz)
		if not self.playeron then
			self.playeron = true
			player.still = player.still + 1
		end
		if (gKeyPressed.left or gKeyPressed.right) then
			self.playeron = false
			player.still = player.still -1
		end
		
		
		fizz.setVelocity(player.fizz,self.speed*self.direction+pvx,pvy)
	else
		if self.playeron then
			self.playeron = false
			player.still = player.still -1
		end
	end
	--]]
	
	fizz.setVelocity(self.fizz,0,self.dy)
	
	if self.clamclosed then
		if self.duration.shell[self.durationtarget] < self.timer.shell then
			self.clamclosed = false
			self.timer.shell = 0
			self.bulletshot = false
		end
	else
		if self.duration.shooting[self.durationtarget] < self.timer.shell then
			self.clamclosed = true
			self.timer.shell = 0
			
			self.durationtarget = self.durationtarget + 1
			
			if self.durationtarget > #self.duration.shell then
				self.durationtarget = 1
			end
			
		end
	end
	
	if not self.clamclosed and (self.timer.shell*10 > self.duration.shooting[self.durationtarget]) and not self.bulletshot then
		self.bulletshot = true
		for tempangle = self.angle - math.pi + math.pi/6, self.angle -math.pi/6,math.pi*2/15 do
			local yoffset = math.sin(tempangle)*self.oy*2 + self.y
			local xoffset = math.cos(tempangle)*self.ox*2 + self.x
			if self.bulletspeed  == -1 then
				entities:create("projectile.defaultorbbullet",{angle = tempangle,x = xoffset,y = yoffset})
			else 
				entities:create("projectile.defaultorbbullet",{angle = tempangle,x = xoffset,y = yoffset,speed = self.bulletspeed})
			end
		end
	end
	
	self:updateanimation()
	for timername, timevalue in pairs(self.timer) do
		self.timer[timername] = timevalue + dt
	end
end

return calmshell