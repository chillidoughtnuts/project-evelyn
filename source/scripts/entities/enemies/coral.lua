local coral = {
		name = "coral",
		x = 0,
		y = 0,
		dx = 0,
		dy = 0,
		angle = 0,
		spritepath = "images/coral.png",
		speed = 100,
		ox = 16,
		oy = 16,
		gravity = 0,
		direction = 1,
		bulletspeed = 100,
		sprites = {},
		playeron = false,
		clamclosed = true,
		bulletshot = true,
		durationtarget = 1,
		durationShellString = "",
		durationShootingString = "",
		duration = {
			shell = {
				5
			},
			shooting = {
				0.2
			}
		},
		timer = {
			shell = 10
		}
	}



function coral:init()
	self.x = tonumber(self.x)
	self.y = tonumber(self.y)
	self.dx = tonumber(self.dx)
	self.dy = tonumber(self.dy)
	self.angle = tonumber(self.angle)
	self.speed = tonumber(self.speed)
	self.ox = tonumber(self.ox)
	self.oy = tonumber(self.oy)
	self.gravity = tonumber(self.gravity)
	self.direction = tonumber(self.direction)
	
	
	self.sprite = love.graphics.newImage(self.spritepath)
	self.ox = 35/2 --self.sprites[1][1]:getWidth()/2
	self.oy = 35/2 --self.sprites[1][1]:getHeight()/2
	
	if self.durationShellString ~= "" then
		local numArray = {}
		for number in self.durationShellString:gmatch("[%d%-]+") do
			table.insert(numArray,tonumber(number))
		end
		self.duration.shell = numArray
	end
	
	if self.durationShootingString ~= "" then
		local numArray = {}
		for number in self.durationShootingString:gmatch("[%d%-]+") do
			table.insert(numArray,tonumber(number))
		end
		self.duration.shooting = numArray
	
	end
	
	while #self.duration.shooting < #self.duration.shell do
		table.insert(self.duration.shooting,1)
	end
	
	self.fizz = fizz.addKinematic("rect", self.x,self.y,self.ox,self.oy)
	self.fizz.gravity = self.gravity
	
end



function coral:draw()
	if self.sprite then
		love.graphics.draw(
			self.sprite,
			self.x,
			self.y,
			self.angle,
			1,
			self.direction,
			self.ox,
			self.oy
		)
	else
		love.graphics.print("coral",self.x,self.y)
	end
end

function coral:update(dt)
	self.x,self.y = fizz.getPosition(self.fizz)
	self.dx,self.dy = fizz.getVelocity(self.fizz)
	local sx,sy = fizz.getDisplacement(self.fizz)
	
	
	
	
	fizz.setVelocity(self.fizz,0,self.dy)
	
	if self.clamclosed then
		if self.duration.shell[self.durationtarget] < self.timer.shell then
			self.clamclosed = false
			self.timer.shell = 0
			self.bulletshot = false
		end
	else
		if self.duration.shooting[self.durationtarget] < self.timer.shell then
			self.clamclosed = true
			self.timer.shell = 0
			
			self.durationtarget = self.durationtarget + 1
			
			if self.durationtarget > #self.duration.shell then
				self.durationtarget = 1
			end
			
		end
	end
	
	if not self.clamclosed and (self.timer.shell*10 > self.duration.shooting[self.durationtarget]) and not self.bulletshot then
		self.bulletshot = true
		for tempangle = self.angle - math.pi + math.pi/6, self.angle -math.pi/6,math.pi*2/15 do
			local yoffset = math.sin(tempangle)*self.oy*2 + self.y
			local xoffset = math.cos(tempangle)*self.ox*2 + self.x
			if self.bulletspeed  == -1 then
				entities:create("projectile.defaultorbbullet",{angle = tempangle,x = xoffset,y = yoffset})
			else 
				entities:create("projectile.defaultorbbullet",{angle = tempangle,x = xoffset,y = yoffset,speed = self.bulletspeed})
			end
		end
	end
	
	for timername, timevalue in pairs(self.timer) do
		self.timer[timername] = timevalue + dt
	end
end

return coral