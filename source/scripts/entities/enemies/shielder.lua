local shielder = {
		name = "shielder",
		x = 0,
		y = 0,
		dx = 0,
		dy = 0,
		angle = 0,
		tilesheetpath = "images/shielder.png",
		speed = 100,
		ox = 16,
		oy = 16,
		gravity = 0,
		direction = -1,
		sprites = {},
		playeron = false,
		shieldup = true,
		bulletshot = true,
		durationtarget = 1,
		durationShieldString = "",
		durationShootingString = "",
		duration = {
			shield = {
				2,1.75,1.5,1.25,1,0.75,0.5,0.25,0.2,0.15,0.1
			},
			shooting = {
				2,1.75,1.5,1.25,1,0.75,0.5,0.25,0.2,0.15,0.1
			}
		},
		timer = {
			shield = 0
		}
	}
	
function shielder:updateanimation()
	if self.shieldup then
		self.currentsprite = self.sprites[2][1]
	else
		self.currentsprite = self.sprites[1][1]
	end
end

function shielder:init()
	self.x = tonumber(self.x)
	self.y = tonumber(self.y)
	self.dx = tonumber(self.dx)
	self.dy = tonumber(self.dy)
	self.angle = tonumber(self.angle)
	self.speed = tonumber(self.speed)
	self.ox = tonumber(self.ox)
	self.oy = tonumber(self.oy)
	self.gravity = tonumber(self.gravity)
	self.direction = tonumber(self.direction)
	
	
	self.tilesheet = love.graphics.newImage(self.tilesheetpath)
	for x = 1, self.tilesheet:getWidth()/42 do
		self.sprites[x] = {}
		for y = 1, self.tilesheet:getHeight()/37 do
			table.insert(
				self.sprites[x],
				y,
				love.graphics.newQuad(
					(x-1)*42,
					(y-1)*37,
					42,
					37,
					self.tilesheet:getWidth(),
					self.tilesheet:getHeight()
				)
			)
		end
	end
	self.currentsprite = self.sprites[1][1]
	self.ox = 42/2--self.sprites[1][1]:getWidth()/2
	self.oy = 37/2--self.sprites[1][1]:getHeight()/2
	
	if self.durationShieldString ~= "" then
		local numArray = {}
		for number in self.durationShootingString:gmatch("[%d%-]+") do
			table.insert(numArray,tonumber(number))
		end
		self.duration.shield = numArray
	end
	
	if self.durationShootingString ~= "" then
		local numArray = {}
		for number in self.durationShootingString:gmatch("[%d%-]+") do
			table.insert(numArray,tonumber(number))
		end
		self.duration.shooting = numArray
	
	end
	while #self.duration.shooting < #self.duration.shield do
		table.insert(self.duration.shooting,1)

	end
	self.fizz = fizz.addKinematic("rect", self.x,self.y,self.ox,self.oy)
	self.fizz.gravity = self.gravity
	
	--[[
	self.fizz.bottom = true
	self.fizz.left = true
	self.fizz.right = true
	self.fizz.top = true
	--]]
end



function shielder:draw()
	if self.currentsprite then
		love.graphics.draw(
			self.tilesheet,
			self.currentsprite,
			self.x,
			self.y,
			self.angle,
			-self.direction,
			1,
			self.ox,
			self.oy
		)
	else
		love.graphics.print("shielder",self.x,self.y)
	end
	
end

function shielder:update(dt)
	self.x,self.y = fizz.getPosition(self.fizz)
	self.dx,self.dy = fizz.getVelocity(self.fizz)
	local sx,sy = fizz.getDisplacement(self.fizz)
	
	local diffx,diffy = player.x-self.x,player.y-self.y
	
	local angleTP = math.atan2(diffx,diffy)
	--self.angle = angleTP
	if angleTP > 0 then
		self.direction = 1
	else
		self.direction = -1
	end
	
	
	--[[turning around when hit something
	if sx > 0 then
		self.direction = 1
	elseif sx < 0 then
		self.direction = -1
	else
		local speeddif = self.speed*self.direction - self.dx
		if  math.abs(speeddif) > self.speed/100 then
			if speeddif > 0 then
				self.direction = 1
			else
				self.direction = -1
			end
		end
	end
	--]]
	
	
	--[[when collide with player
	local nx, ny, pen = fizz.isCollide(self.fizz,player.fizz)
	if (nx or ny or pen) and nx == 0 then
		local pvx,pvy = fizz.getVelocity(player.fizz)
		if not self.playeron then
			self.playeron = true
			player.still = player.still + 1
		end
		if (gKeyPressed.left or gKeyPressed.right) then
			self.playeron = false
			player.still = player.still -1
		end
		
		
		fizz.setVelocity(player.fizz,self.speed*self.direction+pvx,pvy)
	else
		if self.playeron then
			self.playeron = false
			player.still = player.still -1
		end
	end
	--]]
	
	fizz.setVelocity(self.fizz,0,self.dy)
	
	if self.shieldup then
		if self.duration.shield[self.durationtarget] < self.timer.shield then
			self.shieldup = false
			self.timer.shield = 0
			self.bulletshot = false
		end
	else
		if self.duration.shooting[self.durationtarget] < self.timer.shield then
			self.shieldup = true
			self.timer.shield = 0
			
			self.durationtarget = self.durationtarget + 1
			
			if self.durationtarget > #self.duration.shield then
				self.durationtarget = 1
			end
			
		end
	end
	
	if not self.shieldup and (self.timer.shield*5 > self.duration.shooting[self.durationtarget]) and not self.bulletshot then
		self.bulletshot = true
		local tempangle = self.angle
		if self.direction < 0 then
			tempangle = tempangle + math.pi
		end
		entities:create(
			"projectile.defaultbullet",{angle = tempangle,x = self.x + self.direction*16,y = self.y}
		)
	end
	
	self:updateanimation()
	for timername, timevalue in pairs(self.timer) do
		self.timer[timername] = timevalue + dt
	end
end

return shielder