local rubberchariot = {
		name = "rubberchariot",
		x = 0,
		y = 0,
		dx = 0,
		dy = 0,
		angle = 0,
		spritepath = "images/biker.png",
		speed = 500,
		ox = 16,
		oy = 16,
		gravity = 5000,
		direction = 1,
		accel = 100
	}
function rubberchariot:init()
	self.x = tonumber(self.x)
	self.y = tonumber(self.y)
	self.dx = tonumber(self.dx)
	self.dy = tonumber(self.dy)
	self.angle = tonumber(self.angle)
	self.speed = tonumber(self.speed)
	self.ox = tonumber(self.ox)
	self.oy = tonumber(self.oy)
	self.gravity = tonumber(self.gravity)
	self.direction = tonumber(self.direction)
	self.accel = tonumber(self.accel)
	
	self.sprite = love.graphics.newImage(self.spritepath)
	self.ox = self.sprite:getWidth()/2
	self.oy = self.sprite:getHeight()/2
	self.fizz = fizz.addDynamic("rect", self.x,self.y,self.ox,self.oy)
	self.fizz.gravity = self.gravity

	--self.dx = self.speed*self.direction
end

function rubberchariot:draw()
	if self.sprite then
		love.graphics.draw(
			self.sprite,
			self.x,
			self.y,
			self.angle,
			-self.direction,
			1,
			self.ox,
			self.oy
		)
	else
		love.graphics.print("rubberchariot",self.x,self.y)
	end
end

function rubberchariot:update(dt)
	self.x,self.y = fizz.getPosition(self.fizz)
	self.dx,self.dy = fizz.getVelocity(self.fizz)
	local sx,sy = fizz.getDisplacement(self.fizz)
	
	if sx*self.direction < 0 then
		self.x = self.x - 200
		self.y = self.y - 128
		fizz.setPosition(self.fizz,self.x,self.y)
	end
	if sy < 0 then
		if math.abs(self.dx)/self.dx ~= self.direction then
			self.dx = self.dx + math.abs(self.accel)*self.direction*200*dt
		else
			self.dx = self.dx + math.abs(self.accel)*self.direction*dt
		end
	end
	
	if math.abs(self.dx) > math.abs(self.speed) then
		self.dx = self.speed*self.direction
	end
	local nx, ny, pen = fizz.isCollide(self.fizz,player.fizz)
	
	if (nx or ny or pen) and ny == 0 then
		--self.direction = -self.direction --temporary action
		--going to explode later
	end
	fizz.setVelocity(self.fizz,self.dx,self.dy)
end

return rubberchariot