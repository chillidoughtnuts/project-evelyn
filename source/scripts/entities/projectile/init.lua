local projectiles = {}

local temppath = ""
local currentDirectory = "scripts/entities/projectile/"
local files = love.filesystem.getDirectoryItems(currentDirectory)
for i = 1, #files do
	temppath = files[i]
	if temppath ~= "init.lua" then
		temppath = string.gsub(temppath,".lua","")
		projectiles[temppath] = require(currentDirectory .. temppath)
	end
end
return projectiles