local zaffreorbbullet = {
	name = "zaffreorbbullet",
	x = 0,
	y = 0,
	dx = 0,
	dy = 0,
	angle = 0,
	spritepath = "images/ZaffreOne/zaffrecannonball.png",
	speed = 200,
	ox = 16,
	oy = 16,
	gravity = 0,
	--bullethit = {} --abandoned bullet detection
}
function zaffreorbbullet:init()
	
	self.sprite = love.graphics.newImage(self.spritepath)
	self.ox = self.sprite:getWidth()/2
	self.oy = self.sprite:getHeight()/2
	self.fizz = fizz.addDynamic("circle", self.x,self.y,(self.ox+self.oy)/2)
	self.fizz.sonly = true
	--self.fizz.phase = true
	self.fizz.gravity = self.gravity
	
end

function zaffreorbbullet:update(dt)
	local xpos,ypos = fizz.getPosition(self.fizz) 
	self.x = xpos
	self.y = ypos
	
	self.dx = math.cos(self.angle) * self.speed
	self.dy = math.sin(self.angle) * self.speed
	
	fizz.setVelocity(self.fizz,self.dx,self.dy)
	
	local xoff,yoff = map.isOffMap(self.x,self.y)
	local sx, sy = fizz.getDisplacement(self.fizz) 
	if xoff or yoff or sx ~= 0 or sy ~= 0 then
		self.destroy = true
	end
	
	--[[ abandoned bullet detection
	for i = 1, #entities do
		if (not self.bullethit[entities[i].name] or self.bullethit[entities[i].name] <= 0) and entities[i].fizz then
			
			local nx, ny, pen = fizz.isCollide(self.fizz,entities[i].fizz)
			if nx or ny or pen then
				if not entities[i].hit then
					entities[i].hit = {}
				end
				if not self.bullethit[entities[i].name] then
					self.bullethit[entities[i].name] = 0
				end
				if not entities[i].hit[self.name] then
					entities[i].hit[self.name] = 0
				end
				self.bullethit[entities[i].name] = self.bullethit[entities[i].name] + 1
				entities[i].hit[self.name] = entities[i].hit[self.name] + 1
			else
				if self.bullethit[entities[i].name] then
					self.bullethit[entities[i].name] = self.bullethit[entities[i].name] - 1
				end
				
				if entities[i].hit and entities[i].hit[self.name] then
					entities[i].hit[self.name] = entities[i].hit[self.name] - 1
				end
			end
		end
	end
	--]]
end

function zaffreorbbullet:draw()

	if self.sprite then
		love.graphics.draw(self.sprite,self.x,self.y,self.angle,1,1,self.ox,self.oy)
	else
		love.graphics.print("zaffreorbbullet",self.x,self.y)
	end
end

return zaffreorbbullet