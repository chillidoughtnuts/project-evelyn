local zaffrehomingbullet = {
	name = "zaffrehomingbullet",
	x = 0,
	y = 0,
	dx = 0,
	dy = 0,
	angle = 0,
	spritepath = "images/ZaffreOne/zaffreflicker.png",
	speed = 500,
	gentlespeed = 30,
	ox = 16,
	oy = 16,
	gravity = 0,
	timer = {
		animation = 0,
		life = 0,
		shake = 0
	},
	statelength = 0,
	state = 0,
	gentletime = 2,
	delaytime = 0.5,
	targetx = 0,
	targety = 0,
	homing = false
}
function zaffrehomingbullet:init()
	
	self.sprite = love.graphics.newImage(self.spritepath)
	self.ox = self.sprite:getWidth()/2
	self.oy = self.sprite:getHeight()/2
	self.fizz = fizz.addKinematic("circle", self.x,self.y,(self.ox+self.oy)/2)
	self.fizz.phase = true
	self.fizz.gravity = self.gravity
end
function zaffrehomingbullet:animate(dt)
	if self.timer.animation > self.statelength then
		self.state = (self.state == 0) and 1 or  0
		self.timer.animation = 0
		if self.state == 0 then
			self.statelength = math.min(math.random()*(1/10)-1/20 + 0.05,1/15)
		else
			self.statelength = math.min(math.random()*(1/10)-1/20 + 0.05,1/15)
		end
	end
end
function zaffrehomingbullet:update(dt)
	local xpos,ypos = fizz.getPosition(self.fizz) 
	self.x = xpos
	self.y = ypos
	if not self.homing then
		self.dx = math.cos(self.angle) * self.gentlespeed
		self.dy = math.sin(self.angle) * self.gentlespeed
		if self.gentletime < self.timer.life then
			self.homing = true
			self.angle = math.atan2(-self.y+player.y,-self.x+player.x)
		end
	else
		if self.timer.life-self.gentletime < self.delaytime/2 then
			self.angle = math.atan2(-self.y+player.y,-self.x+player.x)
		
		end
		if self.timer.life-self.gentletime < self.delaytime then
			if self.timer.shake > 0.001 then
				self.direction = math.random(-1,1)
				self.direction = self.direction
				self.timer.shake = 0
			end
		else
			self.direction = 1
		end
		self.dx = self.direction*math.cos(self.angle) * self.speed
		self.dy = self.direction*math.sin(self.angle) * self.speed
		
	end
	
	
	
	
	fizz.setVelocity(self.fizz,self.dx,self.dy)
	
	local xoff,yoff = map.isOffMap(self.x,self.y)
	if xoff or yoff or self.destroy then
		return "destroy"
	end
	
	self:animate(dt)
	--[[ abandoned bullet detection
	for i = 1, #entities do
		if (not self.bullethit[entities[i].name] or self.bullethit[entities[i].name] <= 0) and entities[i].fizz then
			
			local nx, ny, pen = fizz.isCollide(self.fizz,entities[i].fizz)
			if nx or ny or pen then
				if not entities[i].hit then
					entities[i].hit = {}
				end
				if not self.bullethit[entities[i].name] then
					self.bullethit[entities[i].name] = 0
				end
				if not entities[i].hit[self.name] then
					entities[i].hit[self.name] = 0
				end
				self.bullethit[entities[i].name] = self.bullethit[entities[i].name] + 1
				entities[i].hit[self.name] = entities[i].hit[self.name] + 1
			else
				if self.bullethit[entities[i].name] then
					self.bullethit[entities[i].name] = self.bullethit[entities[i].name] - 1
				end
				
				if entities[i].hit and entities[i].hit[self.name] then
					entities[i].hit[self.name] = entities[i].hit[self.name] - 1
				end
			end
		end
	end
	--]]
	
	
	--increment timers
	for timername, timevalue in pairs(self.timer) do
		self.timer[timername] = timevalue + dt
	end
end

function zaffrehomingbullet:draw()

	if self.sprite then
		if self.state == 1 then
			love.graphics.draw(self.sprite,self.x,self.y,self.angle,1,1,self.ox,self.oy)
		end
	else
		love.graphics.print("zaffrehomingbullet",self.x,self.y)
	end
end

return zaffrehomingbullet