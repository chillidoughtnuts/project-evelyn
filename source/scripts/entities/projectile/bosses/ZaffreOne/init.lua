local ZaffreOne = {}

local temppath = ""
local currentDirectory = "scripts/entities/projectile/bosses/ZaffreOne/"
local files = love.filesystem.getDirectoryItems(currentDirectory)
for i = 1, #files do
	temppath = files[i]
	if temppath ~= "init.lua" then
		temppath = string.gsub(temppath,".lua","")
		ZaffreOne[temppath] = require(currentDirectory .. temppath)
	end
end
return ZaffreOne