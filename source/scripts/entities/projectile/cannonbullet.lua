local cannonbullet = {
	name = "cannonbullet",
	x = 0,
	y = 0,
	dx = 0,
	dy = 0,
	angle = 0,
	spritepath = "images/cannonshot.png",
	speed = 1,
	ox = 16,
	oy = 16,
	gravity = 1,
	starting = {
		x = 0,
		y = 0,
		angle = 0
	},
	target = {
		x = 32,
		y = 32,
		angle = math.pi/2
	},
	decel = 1
}
function cannonbullet:init()
	
	
	self.sprite = love.graphics.newImage(self.spritepath)
	self.ox = self.sprite:getWidth()/2
	self.oy = self.sprite:getHeight()/2
	self.fizz = fizz.addDynamic("circle", self.x,self.y,(self.ox+self.oy)/2)
	self.fizz.sonly = true
	
	self.fizz.gravity = self.gravity
	local xdiff = math.abs(self.starting.x - self.target.x)
	local ydiff = math.abs(self.starting.y - self.target.y)
	
	local ytimetaken = math.abs((ydiff*2)/self.gravity)^0.5
	local xtimetaken = math.abs(xdiff/ self.speed)
	
	self.dx = math.cos(self.angle) * self.speed
	self.dy = math.sin(self.angle) * self.speed
	self.speed = math.abs((xdiff+math.abs(0.5*self.decel*ytimetaken^2))/ytimetaken)
	
	self.dx = math.cos(self.angle)*self.speed
	self.dy = math.sin(self.angle)*self.speed
	
	self.decel = math.abs(self.decel) * math.abs(self.dx)/-self.dx
	fizz.setPosition(self.fizz,self.x,self.y)
	fizz.setVelocity(self.fizz,self.dx,self.dy)
	
end
function cannonbullet:update(dt)
	self.x,self.y = fizz.getPosition(self.fizz)
	self.dx,self.dy = fizz.getVelocity(self.fizz)
	
	self.dx = self.dx + (self.decel*dt)
	
	
	if self.dx ~= 0 and math.abs(self.dx)/self.dx == math.abs(self.decel)/self.decel then
		self.dx = 0
	end
	
	fizz.setVelocity(self.fizz,self.dx,self.dy)
	
	local xoff,yoff = map.isOffMap(self.x,self.y)
	local sx, sy = fizz.getDisplacement(self.fizz) 
	if xoff or yoff or sx ~= 0 or sy ~= 0 then
		self.destroy = true
	end
	
end

function cannonbullet:draw()

	if self.sprite then
		love.graphics.draw(self.sprite,self.x,self.y,self.angle,1,1,self.ox,self.oy)
	else
		love.graphics.print("cannonbullet",self.x,self.y)
	end
	
end

return cannonbullet