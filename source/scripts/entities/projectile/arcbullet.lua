local arcbullet = {
	name = "arcbullet",
	x = 0,
	y = 0,
	dx = 0,
	dy = 0,
	angle = 0,
	spritepath = "images/cannonshot.png",
	speed = 300,
	ox = 16,
	oy = 16,
	gravity = 0,
	starting = {
		x = 0,
		y = 0,
		angle = 0
	},
	target = {
		x = 32,
		y = 32,
		angle = math.pi/2
	},
	anglecounter = 0,
	anglechange = 0
}
function arcbullet:init()
	
	
	self.sprite = love.graphics.newImage(self.spritepath)
	self.ox = self.sprite:getWidth()/2
	self.oy = self.sprite:getHeight()/2
	self.fizz = fizz.addKinematic("circle", self.x,self.y,(self.ox+self.oy)/2)
	self.fizz.phase = true
	self.fizz.gravity = self.gravity
	
	local xdiff = math.abs(self.starting.x - self.target.x)
	local ydiff = math.abs(self.starting.y - self.target.y)
	if xdiff > ydiff then
		
		self.anglechange = (self.target.angle - self.angle)/((ydiff*math.pi)/(2*self.speed))
		
	else
		
		self.anglechange = (self.target.angle - self.angle)/((xdiff*math.pi)/(2*self.speed))
	
	end
end
function arcbullet:update(dt)
	local xpos,ypos = fizz.getPosition(self.fizz) 
	self.x = xpos
	self.y = ypos
	
	local xdiff = math.abs(self.x - self.target.x)
	local ydiff = math.abs(self.y - self.target.y)
	
	if math.abs(self.anglecounter) > math.abs(self.target.angle-self.starting.angle) or xdiff>ydiff then
		
	else
		self.anglecounter = self.anglecounter + self.anglechange*dt
		self.angle = self.angle + self.anglechange*dt
	end
	self.dx = math.cos(self.angle) * self.speed
	self.dy = math.sin(self.angle) * self.speed
	
	fizz.setVelocity(self.fizz,self.dx,self.dy)
	
	local xoff,yoff = map.isOffMap(self.x,self.y)
	if xoff or yoff then
		return "destroy"
	end
	
end

function arcbullet:draw()

	if self.sprite then
		love.graphics.draw(self.sprite,self.x,self.y,self.angle,1,1,self.ox,self.oy)
	else
		love.graphics.print("arcbullet",self.x,self.y)
	end
end

return arcbullet