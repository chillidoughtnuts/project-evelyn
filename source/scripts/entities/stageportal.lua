local stageportal = {
		name = "stageportal",
		x = 0,
		y = 0,
		ox = 16,
		oy = 16,
		exitmap = "none",
		exitportal = "none",
		timer = {
			switchmap = 0
		}
	}
function stageportal:init()
	
	
	self.x = tonumber(self.x)
	self.y = tonumber(self.y)
	self.ox = tonumber(self.ox)
	self.oy = tonumber(self.oy)
	
	if self.keepX and self.keepX == "false" then
		self.keepX = false
	end
	
	if self.keepY and self.keepY == "false" then
		self.keepY = false
	end
	
	if map.enterpath == self.name then
		local px,py = fizz.getPosition(player.fizz)
		
		if not self.keepY then
			py = self.y
		end
		if not self.keepX then
			px = self.x
		end
		
		fizz.setPosition(player.fizz,px,py)
		fizz.setVelocity(player.fizz,0,0)
	end
	
	
	self.fizz = fizz.addKinematic("rect",self.x,self.y,self.ox,self.oy)	
	
	self.fizz.top = true
	self.fizz.bottom = true
	self.fizz.left = true
	self.fizz.right = true
	
end

function stageportal:draw()
	
end

function stageportal:update(dt)
	local nx,ny,pen = fizz.getTest(self.fizz,player.fizz,dt)
	if  pen and self.timer.switchmap > 0.0 then 
		local tmp = map.searchMapPaths(self.exitmap)
		if tmp then
			map.targetmappath = tmp
			map.enterpath = self.exitportal
			map.reloadmap = true
			self.timer.switchmap = 0
			
			local xspd,yspd = fizz.getVelocity(player.fizz) --get velocity of player
			player.override.xspd = xspd
			player.override.yspd = yspd
		end
	end
	self.timer.switchmap = self.timer.switchmap + dt
end

return stageportal