local entities = {}
local entityscripts = {}
local temppath = ""
local currentDirectory = "scripts/entities/"
local files = love.filesystem.getDirectoryItems(currentDirectory)
for i = 1, #files do
	temppath = files[i]
	if temppath ~= "init.lua" then
		temppath = string.gsub(temppath,".lua","")
		entityscripts[temppath] = require(currentDirectory .. temppath)
	end
end



function entities:update(dt)
	
	for i = 1, #self do
		if self[i] then
			local commands = self[i]:update(dt) or ""
			if (commands and commands == "destroy") or self[i].destroy then
			
				if self[i].fizz then
					fizz.removeShape(self[i].fizz)
				end
				destroy(self[i])
				table.remove(self,i)
			
			end
		end
	end
end

function entities:init()
	for i, d in pairs(self) do
		if type(d) ~= "function" then 
			self[i]:init()
		end
	end
end
function entities:draw()
	for i = 1, #entities do
		self[i]:draw()
	end
	local bulletcount = 0
	for i ,q in pairs(self) do
		if type(q) ~= "function" and self[i] then
			if self[i].name == "cannonbullet" then
				bulletcount = bulletcount + 1
			end
		end
	end
	love.graphics.print(bulletcount,0,110)
end
--
function entities:search(name)
	for i = 1, #self do
		if string.gsub(self[i].name," ","") == string.gsub(name," ","") then
			return self[i]
		end
	end
	return false
end

function entities:destroy()
	for i,d in pairs(self) do
		if self[i] and type(d) ~= "function" then
			
			if self[i].destroyfizz then
				self[i]:destroyfizz()
			elseif self[i].fizz then
				fizz.removeShape(self[i].fizz)
			end
			
			destroy(self[i])
			self[i] = nil
		end
	end
	
	self = {}
end
function entities.testprint()
	local f = 0
	for q,e in pairs(entities) do
		if type(q) == "number" then
			love.graphics.print(q,0,f)--[[
			for c,d in pairs(e) do
				love.graphics.print(c,128,f)
				love.graphics.print(type(d),248,f)
				f = f + 20
			end]]
			f = f + 20
		end
	end
end
--
function entities:create(path,properties)
	--get the actual script from the entityscripts table
	local temporaryscript = entityscripts
	
	if type(path) == "string" then
		for currentscriptpath in string.gmatch(path,"%w+") do
			temporaryscript = temporaryscript[currentscriptpath]
		end
	end
	
	temporaryscript = table.deepcopy(temporaryscript)
	
	if type(properties) == "table" then
		for propertyname,propertyvalue in pairs(properties) do
			temporaryscript[propertyname] = propertyvalue
		end
	end
	
	
	temporaryscript:init()
	
	table.insert(self,temporaryscript)
end
function entities:load()
	local objectlist = {}
	for layerName, field in pairs(map.sti.layers) do --cycle through layers in map
		for layerField, fieldList in pairs(field) do --cycle through layer fields in layer
			if layerField == "name" and fieldList == "Entities" then --if layer field is name and field value is Entities (if name is entities)
				objectlist = field.objects --store table of entities into object list
			end
		end
	end
	
	for i = 1, #objectlist do
		local scriptvalue
		local temporaryscript = entityscripts
		local propertytable = {}
		--get the value of the "script" property in the Tiled map
		for propertyname,propertyvalue in pairs(objectlist[i].properties) do 
			if propertyname == "script" then
				scriptvalue = propertyvalue
			end
		end
		if scriptvalue then
			--get the actual script from the entityscripts table
			if type(scriptvalue) == "string" then
				for currentscriptpath in string.gmatch(scriptvalue,"%w+") do
					temporaryscript = temporaryscript[currentscriptpath]
				end
			end
			temporaryscript = table.deepcopy(temporaryscript)
			for propertyname,propertyvalue in pairs(objectlist[i].properties) do 
				if propertyname ~= "script" then
					temporaryscript[propertyname] = propertyvalue
				end
			end
			
			local tempname = objectlist[i].name
			if tempname then
				temporaryscript.name = tempname
			end
			temporaryscript.x = objectlist[i].x+objectlist[i].width/2
			temporaryscript.y = objectlist[i].y+objectlist[i].height/2
			temporaryscript.ox = objectlist[i].width/2
			temporaryscript.oy = objectlist[i].height/2
			table.insert(self,temporaryscript)
		end
	end
end
return entities