local bosses = {}

local temppath = ""
local currentDirectory = "scripts/entities/bosses/"
local files = love.filesystem.getDirectoryItems(currentDirectory)
for i = 1, #files do
	temppath = files[i]
	if temppath ~= "init.lua" then
		temppath = string.gsub(temppath,".lua","")
		bosses[temppath] = require(currentDirectory .. temppath)
	end
end

return bosses