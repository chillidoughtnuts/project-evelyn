
local ZaffreOne = {
	name = "ZaffreOne",
	x = 0,
	y = 0,
	dx = 0,
	dy = 0,
	angle = 0,
	centralpath = "images/ZaffreOne/central.png",
	cannonpath = "images/ZaffreOne/cannon.png",
	tilesheet = {},
	speed = 500, -- in pixels(relative) per second
	acceleration = 300, -- in pixels per second squared
	sprite = {},
	ox = 16,
	oy = 16,
	gravity = 1,
	pathString = "",
	paths = {{x = 0, y = 0},{x = 32, y = 0}},
	speedchoice = {150,200,100},
	accelchoice = {50,500,50},
	target = 1,
	switch = true,
	centralshootinterval = 1,
	personshootinterval = 1,
	landduration = 120,
	stage = 1,
	floorlevel = 100,
	timer = {
		getpoint = 10,
		shoot = 0,
		land = 0,
		down = 0,
		attack = 0,
		animation = 0,
		death = 0
	},
	direction = 1,
	health = {
		stageone = 10,
		main = 10
	},
	cannon = {
		yoffset = 0
	},
	attack = 1,
	death = {
		x = 0,
		y = 0,
		sprite = {
		
		}
	}
	
	
}

function ZaffreOne:destroyfizz()
	for q,e in pairs(self.fizz) do
		if e then
			fizz.removeShape(self.fizz[q])
		end
	end
end

function ZaffreOne:collisionchange(state) 
	for q,e in pairs(self.fizz) do
		if q ~= "death" then
			self.fizz[q].top = state
			self.fizz[q].bottom = state
			self.fizz[q].left = state
			self.fizz[q].right = state
		end
	end
	self.fizz.exposed.top = true
	self.fizz.exposed.bottom = true
	self.fizz.exposed.right = true
	self.fizz.exposed.left = true
end
function ZaffreOne:insertAttacks()
	self.shoot = {}
	self.shoot.central = {}
	
	table.insert(
		self.shoot.central,
		function()
			if self.timer.shoot > self.centralshootinterval*1 then
				
				local randomnum = math.random()-0.5
				local adjustangle = randomnum*math.pi/16
				
				adjustangle = 0
				
				local tempangle = math.atan2(-self.y+player.y,-self.x+player.x)
				local yoffset = math.sin(tempangle)*self.oy*2 + self.y
				local xoffset = math.cos(tempangle)*self.ox*2 + self.x
				entities:create("projectile.bosses.ZaffreOne.zaffreorbbullet",{angle = (math.pi/2)+adjustangle,x = self.x,y = self.y + self.oy*2})
				self.timer.shoot = 0
			end
		end
	)
	table.insert(
		self.shoot.central,
		function()
			if self.timer.shoot > self.centralshootinterval*1 then
				local randomnum = math.random()-0.5
				local adjustangle = randomnum*math.pi/6
				local tempangle = math.atan2(-self.y+player.y,-self.x+player.x) +adjustangle
				
				local yoffset = math.sin(tempangle)*self.oy*2 + self.y
				local xoffset = math.cos(tempangle)*self.ox*2 + self.x
				anglecount = tempangle
				if tempangle > math.pi-math.pi/16 or tempangle < 0+math.pi/16 then
					if tempangle > -math.pi/2 and tempangle < math.pi/2 then
						tempangle = math.pi/16
					else
						
						tempangle = math.pi-math.pi/16
					end
				end
				
				entities:create("projectile.bosses.ZaffreOne.zaffreorbbullet",{angle = tempangle,x = self.x,y = self.y + self.oy*2})
				self.timer.shoot = 0

			end
		end
	)
	
	self.shoot.person = {}
	table.insert(
		self.shoot.person,
		function()
			if self.timer.shoot > self.personshootinterval*1 then
				local tempangle = math.atan2(-(self.y-19)+player.y,-self.x+player.x)
				local yoffset = math.sin(tempangle)*self.oy*2 + self.y
				local xoffset = math.cos(tempangle)*self.ox*2 + self.x
				entities:create("projectile.bosses.ZaffreOne.zaffreflickerbullet",{angle = tempangle,tempangle,x = self.x,y = self.y-19, speed = 100})
				self.timer.shoot = 0
			end
			if self.timer.attack > self.personshootinterval*1*3 then
				self.timer.attack = 0
				self.attack = math.random(1,2)
			end
		end
	)
	table.insert(
		self.shoot.person,
		function()
			if self.timer.shoot > self.personshootinterval*4 then
				for tempangle = -math.pi/8,-math.pi+math.pi/8,-math.pi/8 do
					local yoffset = math.sin(tempangle)*self.oy*2 + self.y
					local xoffset = math.cos(tempangle)*self.ox*2 + self.x
					entities:create("projectile.bosses.ZaffreOne.zaffrehomingbullet",{angle = tempangle,tempangle,x = self.x,y = self.y-19})
				end
				self.timer.shoot = 0
			end
			if self.timer.attack > self.personshootinterval*4*2.9 then
				self.timer.attack = 0
				self.attack = math.random(1,2)
			end
		end
	)
end
function ZaffreOne:init()
	
	self.x = tonumber(self.x)
	self.y = tonumber(self.y)
	self.dx = tonumber(self.dx)
	self.dy = tonumber(self.dy)
	self.angle = tonumber(self.angle)
	self.speed = tonumber(self.speed)
	self.ox = tonumber(self.ox)
	self.oy = tonumber(self.oy)
	self.gravity = tonumber(self.gravity)
	self.acceleration = tonumber(self.acceleration)
	self.target = tonumber(self.target)
	
	self.floorlevel = tonumber(self.floorlevel)
	
	self.recordspeed = self.speed
	self.tilesheet.central = love.graphics.newImage(self.centralpath)
	self.sprite.cannon = love.graphics.newImage(self.cannonpath)
	
	self.sprite.central = {}
	for x = 1, self.tilesheet.central:getWidth()/85 do
		self.sprite.central[x] = {}
		for y = 1, self.tilesheet.central:getHeight()/66 do
			table.insert(
				self.sprite.central[x],
				y,
				love.graphics.newQuad(
					(x-1)*85,
					(y-1)*66,
					85,
					66,
					self.tilesheet.central:getWidth(),
					self.tilesheet.central:getHeight()
				)
			)
		end
	end
	self.sprite.central.current = self.sprite.central[1][2]
	self.ox = 47/2
	self.oy = 45/2
	self.fizz = {}
	self.fizz.central = fizz.addKinematic("rect", self.x,self.y,self.ox,self.oy)
	
	self.fizz.exposed = fizz.addKinematic("rect", self.x,self.y,26/2,36/2)
	
	self.fizz.sidebottom = fizz.addKinematic("rect",self.x,self.y,81/2,14/2)
	self.fizz.sidemiddle = fizz.addKinematic("rect",self.x,self.y,71/2,10/2)
	self.fizz.sidetop = fizz.addKinematic("rect",self.x,self.y,55/2,7/2)
	self.fizz.cannon = fizz.addKinematic("rect",self.x,self.y,31/2,11/2)
	
	
	self.fizz.death = fizz.addDynamic("rect",self.x,self.y-26,22,22)
	self.fizz.death.top = true
	self.fizz.death.bottom = true
	self.fizz.death.left = true
	self.fizz.death.right = true
	self.fizz.death.sonly = true
	self.fizz.death.gravity = 100
	self.fizz.death.friction = 0.5
	
	if self.pathString ~= "" then
		local numArray = {}
		local count1,count2 = 1,1
		for number in self.pathString:gmatch("[%d%-]+") do
			if not numArray[count1] then
				numArray[count1] = {}
			end
			local key = "none"
			if count2 == 1 then
				key = "x"
			else
				key = "y"
			end
			numArray[count1][key] = tonumber(number)
			count2 = count2 + 1
			if count2 > 2 then
				count1 = count1 + 1
				count2 = 1
			end
		end
		self.paths = numArray
	end
	self:collisionchange(false)
	self:insertAttacks()
	
	
	self.death.tilesheet = love.graphics.newImage("images/ZaffreOne/ground.png")
	for x = 1, self.death.tilesheet:getWidth()/73 do
		self.death.sprite[x] = {}
		for y = 1, self.death.tilesheet:getHeight()/22 do
			table.insert(
				self.death.sprite[x],
				y,
				love.graphics.newQuad(
					(x-1)*73,
					(y-1)*22,
					73,
					22,
					self.death.tilesheet:getWidth(),
					self.death.tilesheet:getHeight()
				)
			)
		end
	end
	
	self.death.sprite.fall = love.graphics.newImage("images/ZaffreOne/fall.png")
	self.death.sprite.fly = love.graphics.newImage("images/ZaffreOne/fly.png")
	
	
end
function ZaffreOne:animate()
	local cl = self.sprite.central
	if self.stage == 1 then
		cl.current = cl[1][1]
	elseif self.stage == 1.25 then
		if self.timer.land > 0.5 then
			cl.current = cl[2][1]
		else
			cl.current = cl[1][1]
		end
	elseif self.stage == 1.5 then
		cl.current = cl[2][1]
	elseif self.stage == 1.75 then
		cl.current = cl[1][2]
	elseif self.stage == 2 then
		if self.timer.animation > 0.1 then
			cl.current = cl.current == cl[2][3] and cl[1][3] or cl[2][3]
			self.timer.animation = 0
		end
	elseif self.stage == 2.5 then
		cl.current = cl[1][1]
	elseif self.stage == 4 then
		cl.current = cl[3][3]
	else
		cl.current = cl[3][2]
	end
end

function ZaffreOne:animatedeath()
	
	local xspd,yspd = fizz.getVelocity(self.fizz.death)
	local dx,dy = fizz.getDisplacement(self.fizz.death)
	if dy >= 0 then
		if yspd < 0 then
			love.graphics.draw(
				self.death.sprite.fly,
				self.fizz.death.x,
				self.fizz.death.y,
				self.angle,
				1,
				1,
				50/2,
				51/2
			)
		else
			
			love.graphics.draw(
				self.death.sprite.fall,
				self.fizz.death.x,
				self.fizz.death.y,
				self.angle,
				1,
				1,
				42/2,
				45/2
			)
		end
	else
		if self.timer.death < 3 then
			love.graphics.draw(
				self.death.tilesheet,
				self.death.sprite[1][1],
				self.fizz.death.x,
				self.fizz.death.y+11,
				self.angle,
				1,
				1,
				73/2,
				22/2
			)
		elseif self.timer.death < 4 then
			love.graphics.draw(
				self.death.tilesheet,
				self.death.sprite[1][2],
				self.fizz.death.x,
				self.fizz.death.y+11,
				self.angle,
				1,
				1,
				73/2,
				22/2
			)
		elseif self.timer.death < 6 then
			love.graphics.draw(
				self.death.tilesheet,
				self.death.sprite[1][3],
				self.fizz.death.x,
				self.fizz.death.y+11,
				self.angle,
				1,
				1,
				73/2,
				22/2
			)
		end
	end
end


function ZaffreOne:draw()
	love.graphics.print(anglecount or "",0,70)
	if self.sprite then
		local centralx,centraly = fizz.getPosition(self.fizz.central)
		local cannonx,cannony = fizz.getPosition(self.fizz.cannon)
		love.graphics.draw(
			self.sprite.cannon,
			self.fizz.cannon.x,
			self.fizz.cannon.y,
			self.angle,
			1,
			1,
			31/2,
			11/2
		)
		love.graphics.draw(
			self.tilesheet.central,
			self.sprite.central.current,
			self.fizz.central.x,
			self.fizz.central.y-10.5,
			self.angle,
			1,
			1,
			85/2,
			66/2
		)
		
		
		
		
	else
		love.graphics.print("ZaffreOne",self.x,self.y)
	end
	if self.stage == 4 then
		self:animatedeath()
	end
	
	love.graphics.setColor(255,0,0)
	
	if self.stage == 2 then
		love.graphics.print(math.floor((self.landduration -self.timer.down)*100)/100,self.x+30,self.y-50)
	end
	love.graphics.print(self.health.main,self.x-50,self.y-50)
	love.graphics.print(self.health.stageone,self.x,self.y-50)
	love.graphics.print("Attack Number: " .. self.attack,self.x-32,self.y-60)
	love.graphics.setColor(255,255,255)
	
end
function ZaffreOne:destroyBullets(dt,exposed)

	for i = 1, #entities do
		if entities[i] and type(entities[i]) ~= "function" then
			
			if entities[i].name == "defaultbullet" then
				if exposed then
					local nx,ny,pen = fizz.getTest(self.fizz.exposed,entities[i].fizz,dt)
					if nx or ny or pen then
						if self.stage == 2 then
							entities[i].destroy = true	
							self.health.main = self.health.main - 1
						end
					end
				else
					if entities[i].fizz and type(entities[i].fizz) == "table" then
						local count = 0
						for name in pairs(self.fizz) do
							--if entities[i].timer.life > 0.1 then
							if name ~= "exposed" and name ~= "death" then
								local nx,ny,pen = fizz.getTest(self.fizz[name],entities[i].fizz,dt)
								if nx or ny or pen then
									entities[i].destroy = true
									self.health.stageone = self.health.stageone - 1
								end
							end
							--end
						end
					end
				end
			end
		end
	end
end

function ZaffreOne:update(dt)
	
	self:animate()
	local xpos,ypos = fizz.getPosition(self.fizz.central) 
	local xspd,yspd = fizz.getVelocity(self.fizz.central)
	self.x = xpos
	self.y = ypos
	if self.stage == 1 or self.stage == 1.25 then
		
		self:destroyBullets(dt)
		
		local target = self.paths[self.target]
		local dx,dy = target.x - self.x, target.y - self.y
		local angle = math.atan2(dy,dx)
		local distance = (dx^2+dy^2)^0.5
		
		local speed = (xspd^2 + yspd^2)^0.5
		
		if math.abs(distance) <= math.abs((speed^2)/self.acceleration) then
			self.direction = -1
		else
			self.direction = 1
		end
		
		
		yspd = yspd + math.sin(angle)*self.acceleration*dt*self.direction
		xspd = xspd + math.cos(angle)*self.acceleration*dt*self.direction
		
		
		
		if speed > self.speed then
			xspd = math.cos(angle)*self.speed
			yspd = math.sin(angle)*self.speed
		end
		if (distance <= 0.2)then
			if self.switch then
				fizz.setPosition(self.fizz.central,
					self.paths[self.target].x,
					self.paths[self.target].y
				)
				if self.target == #self.paths then
					self.target = 1
				else
					self.target = self.target + 1
				end
				xspd,yspd = 0,0
				
				--
				local randomnum = math.random(#self.speedchoice)
				
				self.speed = self.speedchoice[randomnum]
				self.acceleration = self.accelchoice[randomnum]
				
				self.attack = math.random(1,2)
				--
			end
			
			self.timer.getpoint = 0
			self.switch = false
		else
			if self.timer.getpoint >  100/self.speed then
				self.switch = true
			end
		end
		
		if self.stage == 1 then
		
			self.shoot.central[self.attack]()
		end
		
		if (gKeyPressed.k or self.health.stageone < 0) and self.stage == 1 then
			self.stage = 1.25
			self.timer.land = 0
			self.direction = -self.direction
		end
		
		
		if self.stage == 1.25 then
			
			local speed = (xspd^2 + yspd^2)^0.5
			--if (speed < math.abs(self.speed)/5  and math.abs(xpos-player.x) < 37) or  math.abs(xpos-player.x) < 2 then
			if self.timer.land > 1 then
				yspd = 0
				
				--[[
				local temp = math.abs(xpos-player.x)
				if temp < 1 then temp = 1 end
				xspd = xspd - 20000*dt*(math.abs(xspd)/xspd)/temp
				]]
				--slow down script
				xspd = 0
				if math.abs(xspd) < 10 then
					self.stage = 1.5
					self.timer.land = 0
					--fizz.removeShape(self.fizz.cannon)
					--self.fizz.cannon = nil
					
					self:collisionchange(true)
				end
				
			end
		end
		
	elseif self.stage == 1.5 then
		
		self:destroyBullets(dt)
		if math.abs(xspd) > 2 then
			xspd = xspd - self.acceleration*dt*(math.abs(xspd)/xspd)
		else
			xspd = 0
		end
		if self.timer.land > 0.1 then
			local gravity = 3000
			yspd = yspd + gravity*dt
			
			if ypos +45/2 > self.floorlevel then
				ypos = self.floorlevel-45/2
				yspd = 0
			end
			
			if xspd == 0 and yspd == 0 then
				self.stage = 1.75
				self.timer.land = 0
			end
		else
			yspd = 0
		end
	elseif self.stage == 1.75 then
		if self.timer.land > 3 then
			self.stage = 2
			--ZEFFRE COMES OUT OF MACHINE ANIMATION
			self.timer.down = 0
			self.attack = 1
		end
	
	elseif self.stage == 2 then
		--ZEFFRE ATTACK ANIMATION
		self.shoot.person[self.attack]()
		
		self:destroyBullets(dt,true)
		if gKeyPressed.l or self.timer.down > self.landduration then
			self.stage = 2.5
			self.health.stageone = 10
		end
	elseif self.stage == 2.5 then
		
		self:destroyBullets(dt,true)
		
		local target = self.paths[self.target]
		local dx,dy = 0, target.y - self.y
		local angle = math.atan2(dy,dx)
		local distance = (dx^2+dy^2)^0.5
		local speed = (xspd^2 + yspd^2)^0.5
		local acel = 100
		local mxspd = 100
		if math.abs(distance) <= math.abs((speed^2)/acel) then
			self.direction = -1
		else
			self.direction = 1
		end
		
		xspd = xspd + math.cos(angle)*acel*dt*self.direction
		yspd = yspd + math.sin(angle)*acel*dt*self.direction
		if speed > mxspd then
			xspd = math.cos(angle)*mxspd
			yspd = math.sin(angle)*mxspd
		end
		
		if (distance <= 0.2)then
			
			fizz.setPosition(self.fizz.central,
				xpos,
				self.paths[self.target].y
			)
			
			xspd,yspd = 0,0
			self.stage = 1
			self.attack = 1
			self:collisionchange(false)
		end
	elseif self.stage == 4 then
		xspd,yspd = 0,0
		local dx,dy = fizz.getDisplacement(self.fizz.death)
		
		if dy == 0 then
			self.timer.death = 0
		end
	end
	
	if self.stage ~= 4 then
		
		fizz.setVelocity(self.fizz.death,0,0)
	end
	if self.health.main < 0 and self.stage ~= 4 or gKeyPressed.h then
		self.stage = 4
		fizz.setVelocity(self.fizz.death,0,-120)
		fizz.setPosition(self.fizz.death,xpos,ypos-26-10)
		self:collisionchange(true)
		xspd,yspd = 0,0
		self.timer.death = 0
		self.fizz.death.top = false
		self.fizz.death.bottom = false
		self.fizz.death.left = false
		self.fizz.death.right = false
	end
	--set central
	fizz.setVelocity(self.fizz.central,xspd,yspd)
	fizz.setPosition(self.fizz.central,xpos,ypos)
	--align collision boxes with central
	if self.fizz.cannon then
		local retractspd = 1
		if self.stage == 1.5 then
			retractspd = 10
		end
		if self.stage ~= 4 then
			if self.stage ~= 1 and self.stage ~= 2.5 then
				self.cannon.yoffset = self.cannon.yoffset + self.sprite.cannon:getHeight() * dt*retractspd
			else
				self.cannon.yoffset = self.cannon.yoffset - self.sprite.cannon:getHeight() * dt*retractspd
			end
		end
		
		if self.cannon.yoffset >= self.sprite.cannon:getHeight() then
			self.cannon.yoffset = self.sprite.cannon:getHeight()
		elseif self.cannon.yoffset < 0 then
			self.cannon.yoffset = 0
		end
		fizz.setPosition(self.fizz.cannon,xpos,ypos+27-self.cannon.yoffset)
	end
	if self.fizz.sidebottom then
		fizz.setPosition(self.fizz.sidebottom,xpos,ypos+15.5)
	end
	if self.fizz.sidemiddle then
		fizz.setPosition(self.fizz.sidemiddle,xpos,ypos+3.5)
	end
	if self.fizz.sidetop then
		fizz.setPosition(self.fizz.sidetop,xpos,ypos-5.5)
	end
	if self.fizz.exposed then
		fizz.setPosition(self.fizz.exposed,xpos,ypos-26)
	end
	if self.fizz.death then
		if self.stage ~= 4 then
			fizz.setPosition(self.fizz.death,xpos,ypos-26)
		end
	end
	
	--increment timers
	for timername, timevalue in pairs(self.timer) do
		self.timer[timername] = timevalue + dt
	end
	
end

return ZaffreOne