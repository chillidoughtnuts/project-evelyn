local platform = {
		name = "platform",
		x = 0,
		y = 0,
		dx = 0,
		dy = 0,
		angle = 0,
		spritepath = "images/blackcubeofdoom.png",
		speed = 100, -- in pixels(relative) per second
		acceleration = 50, -- in pixels per second squared
		ox = 16,
		oy = 16,
		gravity = 0,
		pathString = "",
		paths = {{x = 0, y = 0}},
		target = 2,
		switch = false,
		timer = {getpoint = 10}
	}
function platform:init()

	self.x = tonumber(self.x)
	self.y = tonumber(self.y)
	self.dx = tonumber(self.dx)
	self.dy = tonumber(self.dy)
	self.angle = tonumber(self.angle)
	self.speed = tonumber(self.speed)
	self.ox = tonumber(self.ox)
	self.oy = tonumber(self.oy)
	self.gravity = tonumber(self.gravity)
	self.acceleration = tonumber(self.acceleration)
	self.target = tonumber(self.target)
	
	self.sprite = love.graphics.newImage(self.spritepath)
	self.ox = self.sprite:getWidth()/2
	self.oy = self.sprite:getHeight()/2
	self.fizz = fizz.addKinematic("rect", self.x,self.y,self.ox,self.oy)
	self.fizz.gravity = self.gravity
	
	
	local numArray = {}
	local count1,count2 = 1,1
	for number in self.pathString:gmatch("[%d%-]+") do
		if not numArray[count1] then
			numArray[count1] = {}
		end
		local key = "none"
		if count2 == 1 then
			key = "x"
		else
			key = "y"
		end
		numArray[count1][key] = tonumber(number)
		count2 = count2 + 1
		if count2 > 2 then
			count1 = count1 + 1
			count2 = 1
		end
	end
	self.paths = numArray
end

function platform:draw()
	if self.sprite then
		love.graphics.draw(
			self.sprite,
			self.x,
			self.y,
			self.angle,
			1,
			1,
			self.ox,
			self.oy
		)
	else
		love.graphics.print("platform",self.x,self.y)
	end
	
end

function platform:update(dt)
	local xpos,ypos = fizz.getPosition(self.fizz) 
	local xspd,yspd = fizz.getVelocity(self.fizz)
	self.x = xpos
	self.y = ypos
	
	
		
		local target = self.paths[self.target]
		local dx,dy = target.x - self.x, target.y - self.y
		local angle = math.atan2(dy,dx)
		local distance = (dx^2+dy^2)^0.5
		
		local speed = (xspd^2 + yspd^2)^0.5
		local flip = 1
		if math.abs(distance) <= math.abs((speed^2)/self.acceleration) then
			flip = -1
		end
		xspd = xspd + math.cos(angle)*self.acceleration*dt*flip
		yspd = yspd + math.sin(angle)*self.acceleration*dt*flip
		
		
		if speed > self.speed then
			
			xspd = math.cos(angle)*self.speed
			yspd = math.sin(angle)*self.speed
		end
		
		if (distance <= 0.1)then
			if self.switch then
				fizz.setPosition(self.fizz,
					self.paths[self.target].x,
					self.paths[self.target].y
				)
				if self.target == #self.paths then
					self.target = 1
				else
					self.target = self.target + 1
				end
				xspd,yspd = 0,0
				
			end
			
			self.timer.getpoint = 0
			self.switch = false
		else
			if self.timer.getpoint >  100/self.speed  then
				self.switch = true
			end
		end
		
	local nx, ny, pen = fizz.isCollide(self.fizz,player.fizz)
	
	
	if (nx or ny or pen) and ny ~= 0 then
		local pvx,pvy = fizz.getVelocity(player.fizz)
		if not self.playeron then
			self.playeron = true
			player.still = player.still + 1
		end
		if (gKeyPressed.left or gKeyPressed.right) then
			self.playeron = false
			player.still = player.still -1
		end
		
			
		fizz.setVelocity(player.fizz,xspd+pvx,pvy)
	else
		if self.playeron then
			self.playeron = false
			player.still = player.still -1
		end
	end
	
	
	fizz.setVelocity(self.fizz,xspd,yspd)
	
	self.timer.getpoint = self.timer.getpoint + dt
	
	
end

return platform