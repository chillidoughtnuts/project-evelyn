local titlescreen = {
	name = "titlescreen",
	button = {}
}

function titlescreen:init()
	self.button.quit = gooi.newButton({
		text = "Quit"
	}):onRelease(function()
        love.event.quit()
    end)
	self.button.go = gooi.newButton({
		text = "Go"
	}):onRelease(function()
        map.targetmappath = map.mappaths.FirstLevel
		
		map.reloadmap = true
    end)
end

function titlescreen:draw()	
	
	gooi.draw()
end
function titlescreen:resize(w,h)
	self.button.quit.w = w/5
	self.button.quit.h = h/8
	self.button.quit.x = (w - self.button.quit.w)/2
	self.button.quit.y = (h - self.button.quit.h)/2 + self.button.quit.h*0.6
	
	self.button.go.w = w/5
	self.button.go.h = h/8
	self.button.go.x = (w - self.button.quit.w)/2
	self.button.go.y = (h - self.button.quit.h)/2 - self.button.quit.h*0.6
end
function titlescreen:update(dt)
	
		
	for q,e in pairs(self.timer or {}) do
		self.timer[q] = e + dt
	end
	
end

return titlescreen