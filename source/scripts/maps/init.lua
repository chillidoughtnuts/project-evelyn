local mapscripts = {}
local maps = {}
local temppath = ""
local currentDirectory = "scripts/maps/"
local files = love.filesystem.getDirectoryItems(currentDirectory)
for i = 1, #files do
	temppath = files[i]
	if temppath ~= "init.lua" then
		temppath = string.gsub(temppath,".lua","")
		maps[temppath] = require(currentDirectory .. temppath)
	end
end



function mapscripts.update(dt)
	for i = 1, #mapscripts do
		mapscripts[i]:update(dt)
	end
end

function mapscripts.init()
	for i = 1, #mapscripts do
		mapscripts[i]:init()
	end
end
function mapscripts.draw()
	for i = 1, #mapscripts do
		mapscripts[i]:draw()
	end

end
function mapscripts.resize(w,h)
	
	for i = 1, #mapscripts do
		if mapscripts[i].resize then
			mapscripts[i]:resize(w,h)
		end
	end
end
--

function mapscripts.destroy()
	
	
	for i = 1, #mapscripts do
		
		if mapscripts[i] then
			destroy(mapscripts[i])
			table.remove(mapscripts,i)
		end
	end
end


function mapscripts.create(path,properties)
	--get the actual script from the maps table
	local temporaryscript = maps
	if type(path) == "string" then
		for currentscriptpath in string.gmatch(path,"%w+") do
			temporaryscript = temporaryscript[currentscriptpath]
		end
	end
	temporaryscript = table.deepcopy(temporaryscript)

	temporaryscript:init()
	table.insert(mapscripts,temporaryscript)
end


function mapscripts.load(script)
	if script then
		local temporaryscript = maps
		if type(script) == "string" then
			for currentscriptpath in string.gmatch(script,"%w+") do
				temporaryscript = temporaryscript[currentscriptpath]
			end
			
		else
			temporaryscript = maps[script]
		end
		
		table.insert(mapscripts,temporaryscript)
	else
		local mapproperties  = map.sti.properties
	
		local scriptvalue
		local temporaryscript = maps
		local propertytable = {}
		--get the value of the "script" property in the Tiled map
		for propertyname,propertyvalue in pairs(mapproperties) do 
			if propertyname == "script" then
				scriptvalue = propertyvalue
			end
		end
		if scriptvalue then
			--get the actual script from the maps table
			if type(scriptvalue) == "string" then
				for currentscriptpath in string.gmatch(scriptvalue,"%w+") do
					temporaryscript = temporaryscript[currentscriptpath]
				end
			end
			
			temporaryscript = table.deepcopy(temporaryscript)
			for propertyname,propertyvalue in pairs(mapproperties) do 
				if propertyname ~= "script" then
					temporaryscript[propertyname] = propertyvalue
				end
			end
			
			
			table.insert(mapscripts,temporaryscript)
		end
	end
end
return mapscripts