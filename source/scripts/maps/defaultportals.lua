local defaultportals = {
	name = "defaultportals",
	sx = 0,
	sy = 0,
	timer = {
		screenshake = 0,
		screenshake2 = 0
	},
	offsetx = 0,
	offsety = 0,
	dialoguedata = {}
}

function defaultportals:init()
	
	self.dialoguedata.poop = dialogue.new("images/dialogue/standard.png","words words words then some words and more words and then even more words will appear",1)
	self.dialoguedata.salt = dialogue.new("images/dialogue/standard.png","Starting with words and more words, some very different words here and some words that really word. Now for some words in order for words to word really long so word scroll word scroll wording scrolling towards the words of sword",0)
end

function defaultportals:draw()	
	love.graphics.translate(math.floor(self.sx),math.floor(self.sy))
	mapscale.scale()
	map.sti:draw()
	entities:draw()
	player.draw()
	
	if gKeyPressed.d then
		self.dialoguedata.poop:draw()
	else
		self.dialoguedata.salt:draw()
	end
	
	------Stuff
	
	love.graphics.setColor(0,100,100)
	love.graphics.print("Frames per Second: " .. love.timer.getFPS(),0,0)
	love.graphics.print("Updates per Second: " .. math.floor(drawPS),0,10)
	local sx, sy = fizz.getVelocity(player.fizz)
	local xpos,ypos = fizz.getPosition(player.fizz)
	love.graphics.print("X Velocity: " .. sx .. " Y Velocity: " .. sy, 0, 20)
	love.graphics.print("X Position: " .. math.floor(xpos) .. " Y Position: " .. math.floor(ypos),0,30)
	love.graphics.print("Timeshift Value: " .. timeshift,0,40)
	
	
end
function defaultportals:resize(w,h)
	mapscale.resizeMap(w,h)
end
function defaultportals:update(dt)
	
	
	
	if timeshift ~= 0 then
		map.update(dt)
		player.update(dt)
		entities:update(dt)
		fizz.update(dt)
		
		if gKeyPressed.m then
			if self.timer.screenshake2 > 0.1 then
				self.offsetx = math.random()*30 - 30/2
				self.offsety = math.random()*30 - 30/2
				self.timer.screenshake2 = 0
			end
		end
		if gKeyPressed.s then
			if self.timer.screenshake > 0.02 then
				self.sx = math.random()*15 - 15/2 +self.offsetx
				self.sy = math.random()*15 - 15/2 +self.offsety
				self.timer.screenshake = 0
			end
		else
			self.sx = 0
			self.sy = 0
		end
		
		for q,e in pairs(self.timer) do
			self.timer[q] = e + dt
		end
	end
end

return defaultportals