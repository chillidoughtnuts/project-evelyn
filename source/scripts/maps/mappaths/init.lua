local mappaths = {

	--Just ignore this stuff, was using it for testing
	TestRoom = "maps/Testing/TestRoom.lua",
	Stage1 = "maps/Testing/Stage1.lua",
	FirstLevel = "maps/Testing/FirstLevel.lua",
	SecondLevel = "maps/Testing/SecondLevel.lua",
	ThirdLevel = "maps/Testing/ThirdLevel.lua",
	FourthLevel = "maps/Testing/FourthLevel.lua",
	FifthLevel = "maps/Testing/FifthLevel.lua",
	SixthLevel = "maps/Testing/SixthLevel.lua",
	SeventhLevel = "maps/Testing/SeventhLevel.lua",
	EighthLevel = "maps/Testing/EighthLevel.lua",
	NinthLevel = "maps/Testing/NinthLevel.lua",
	TenthLevel = "maps/Testing/TenthLevel.lua",
	EleventhLevel = "maps/Testing/EleventhLevel.lua",
	TwelvthLevel = "maps/Testing/TwelvthLevel.lua"
	
	--Let us begin here
}
return mappaths