local dialogue = {
	
}


local drawtext = function(text,placement,scale,step)
	local r,g,b,a = love.graphics.getColor()
	local mapw,maph = map.getSize()
	local widthallowed = mapw-(96*scale*2+32+32)

	local font = love.graphics.getFont()
	local linew = font:getWidth(text)
	local fontheight = font:getHeight()
	local textwidth,displaytext = font:getWrap(text,widthallowed/scale)
	
	local linestart = 0
	local rectstart = 0
	if placement and placement > 0 then
		linestart = 32+96*scale
		rectstart = linestart
	else
		--linestart = mapw-32-96*scale-linew*scale
		linestart = 32+96*scale
		rectstart = 32+96*scale
	end
	
	love.graphics.setColor(0,0,0)
	love.graphics.rectangle("fill",rectstart,32,widthallowed,fontheight*scale*4)--64*scale)
	
	love.graphics.setColor(255,255,255)
	local heightposition = 32
	
	for i = step, math.min(step+3,#displaytext) do
		love.graphics.print(displaytext[i],linestart,32 + fontheight*scale*(i-step),0,scale,scale,0,0)
	end
	if step+3 >= #displaytext then
		return 1
	else
		return 0,rectstart,32,widthallowed,fontheight*scale*4
	end
	love.graphics.setColor(r,g,b)
	
end
dialogue.new = function(imagepath,displaytext,number)
	local dialoguedata = {
		portraitsprite = love.graphics.newImage(imagepath) or love.graphics.newImage("images/dialogue/standard.png"),
		placement = number or 1,
		displaytext = displaytext or "",
		step = 1,
		pressed = false,
		fin = false
	}
	
	function dialoguedata:draw(x,y)
		local mapw,maph = map.getSize()
		local scale = math.min((mapw/4)/96,math.max(1.5/mapscale.scaleFactor,mapw/512))
		if x or y then
		
		else
			if self.placement and self.placement > 0 then
				if self.portraitsprite then
					love.graphics.draw(self.portraitsprite,32,32,0,scale,scale,0,0)
				end
			else
				if self.portraitsprite then
					love.graphics.draw(self.portraitsprite,mapw-32,32,0,scale,scale,96,0)
				end
			end
			
			
			local display,rx,ry,rw,rh = drawtext(self.displaytext,self.placement,scale,self.step)
			
			
			if love.mouse.isDown(1,2,3) then
				if not self.pressed then
					if display == 0 then
						local mx,my = love.mouse.getPosition()
						if rx < mx and rw+rx > mx then
							
							if ry < my and rh+ry > my then
								
								--love.graphics.print("safsd",120,100)
								self.pressed = true
								self.step = self.step + 1
							end
						end
					else
						self.fin = true
						love.graphics.print('ssdg',120,120)
					end
				end
			else
				self.pressed = false
			end
			
		end
		
		if self.fin then
			return "end"
		end
	end
	
	return dialoguedata
end

return dialogue