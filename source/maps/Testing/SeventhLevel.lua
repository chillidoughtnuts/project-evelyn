return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "0.15.2",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 16,
  height = 9,
  tilewidth = 32,
  tileheight = 32,
  nextobjectid = 41,
  properties = {
    ["entrance"] = "Giancoli",
    ["name"] = "Stage1",
    ["script"] = "defaultportals"
  },
  tilesets = {
    {
      name = "Clouds",
      firstgid = 1,
      tilewidth = 32,
      tileheight = 32,
      spacing = 1,
      margin = 1,
      image = "../../images/Clouds.png",
      imagewidth = 208,
      imageheight = 117,
      transparentcolor = "#ffaec9",
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 18,
      tiles = {}
    },
    {
      name = "tilesheet",
      firstgid = 19,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../../images/tilesheet.png",
      imagewidth = 320,
      imageheight = 160,
      transparentcolor = "#ffaec9",
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 50,
      tiles = {
        {
          id = 0,
          properties = {
            ["polygon"] = "11,0.10,1 .. 32, 0 . 11,0   .. 32, 32. 32, 0  .. 10,2. 24,16.. 24,16 . 32,32 .. 10,1 . 10,2 .. 30,28 . 32,28"
          }
        },
        {
          id = 1,
          properties = {
            ["polygon"] = "22,1.21,0 .. 21,0 . 0, 0   .. 0,0 .0, 32  ..  8,16.22,2 .. 0,32. 8,16  .. . 22,2. 22,1  .. 0,28. 2,28 ."
          }
        },
        {
          id = 37,
          properties = {
            ["polygon"] = "32,0 .. 0,0"
          }
        }
      }
    }
  },
  layers = {
    {
      type = "imagelayer",
      name = "Background",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      image = "../../images/beach background.png",
      properties = {}
    },
    {
      type = "tilelayer",
      name = "Tile Layer 3",
      x = 0,
      y = 0,
      width = 16,
      height = 9,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 16, 0, 0, 0, 0, 0, 16, 0, 0, 0, 0, 0, 16,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 41, 23,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 29, 23, 23, 23,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23, 23, 23, 23,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    },
    {
      type = "tilelayer",
      name = "Tile Layer 2",
      x = 0,
      y = 0,
      width = 16,
      height = 9,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {
        ["Collidable"] = "true"
      },
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 49, 22,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 49, 49, 23, 23,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 29, 23, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 49, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 29, 23, 23, 23, 41,
        21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 23, 41, 23, 23, 23
      }
    },
    {
      type = "objectgroup",
      name = "Entities",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      objects = {
        {
          id = 26,
          name = "Kerman",
          type = "",
          shape = "rectangle",
          x = -32,
          y = -50.5,
          width = 32,
          height = 367,
          rotation = 0,
          visible = true,
          properties = {
            ["exitmap"] = "SixthLevel",
            ["exitportal"] = "Yerman",
            ["script"] = "stageportal"
          }
        },
        {
          id = 28,
          name = "Bopper",
          type = "",
          shape = "rectangle",
          x = 0,
          y = -57.25,
          width = 32,
          height = 370.5,
          rotation = 0,
          visible = true,
          properties = {
            ["keepY"] = "true",
            ["script"] = "stageportal"
          }
        },
        {
          id = 34,
          name = "",
          type = "",
          shape = "rectangle",
          x = 128,
          y = 33,
          width = 0,
          height = 15,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 35,
          name = "",
          type = "",
          shape = "rectangle",
          x = 127,
          y = 35,
          width = 35,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 36,
          name = "",
          type = "",
          shape = "rectangle",
          x = 161,
          y = 35,
          width = 1,
          height = 63,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 37,
          name = "RubberChariot1",
          type = "",
          shape = "rectangle",
          x = 35.5,
          y = 67.5,
          width = 41,
          height = 29,
          rotation = 0,
          visible = true,
          properties = {
            ["script"] = "enemies.rubberchariot"
          }
        },
        {
          id = 39,
          name = "Zeus",
          type = "",
          shape = "rectangle",
          x = 512,
          y = -97,
          width = 32,
          height = 346.5,
          rotation = 0,
          visible = true,
          properties = {
            ["exitmap"] = "EighthLevel",
            ["exitportal"] = "Hermes",
            ["script"] = "stageportal"
          }
        },
        {
          id = 40,
          name = "Salamander",
          type = "",
          shape = "rectangle",
          x = 480,
          y = 118.993,
          width = 32,
          height = 121.615,
          rotation = 0,
          visible = true,
          properties = {
            ["keepY"] = "true",
            ["script"] = "stageportal"
          }
        }
      }
    }
  }
}
