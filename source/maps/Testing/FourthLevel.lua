return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "0.15.2",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 16,
  height = 9,
  tilewidth = 32,
  tileheight = 32,
  nextobjectid = 39,
  properties = {
    ["entrance"] = "Giancoli",
    ["name"] = "Stage1",
    ["script"] = "defaultportals"
  },
  tilesets = {
    {
      name = "Clouds",
      firstgid = 1,
      tilewidth = 32,
      tileheight = 32,
      spacing = 1,
      margin = 1,
      image = "../../images/Clouds.png",
      imagewidth = 208,
      imageheight = 117,
      transparentcolor = "#ffaec9",
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 18,
      tiles = {}
    },
    {
      name = "tilesheet",
      firstgid = 19,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../../images/tilesheet.png",
      imagewidth = 320,
      imageheight = 160,
      transparentcolor = "#ffaec9",
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 50,
      tiles = {
        {
          id = 0,
          properties = {
            ["polygon"] = "11,0.10,1 .. 32, 0 . 11,0   .. 32, 32. 32, 0  .. 10,2. 24,16.. 24,16 . 32,32 .. 10,1 . 10,2 .. 30,28 . 32,28"
          }
        },
        {
          id = 1,
          properties = {
            ["polygon"] = "22,1.21,0 .. 21,0 . 0, 0   .. 0,0 .0, 32  ..  8,16.22,2 .. 0,32. 8,16  .. . 22,2. 22,1  .. 0,28. 2,28 ."
          }
        },
        {
          id = 37,
          properties = {
            ["polygon"] = "32,0 .. 0,0"
          }
        }
      }
    }
  },
  layers = {
    {
      type = "imagelayer",
      name = "Background",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      image = "../../images/beach background.png",
      properties = {}
    },
    {
      type = "tilelayer",
      name = "Tile Layer 3",
      x = 0,
      y = 0,
      width = 16,
      height = 9,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 16,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    },
    {
      type = "tilelayer",
      name = "Tile Layer 2",
      x = 0,
      y = 0,
      width = 16,
      height = 9,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {
        ["Collidable"] = "true"
      },
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 49,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 29,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 19, 20, 0, 0, 0, 0, 29,
        0, 0, 0, 0, 0, 0, 19, 20, 0, 0, 0, 0, 0, 0, 0, 29,
        0, 0, 0, 0, 19, 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 29,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 49, 23,
        21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 23, 23
      }
    },
    {
      type = "objectgroup",
      name = "Entities",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      objects = {
        {
          id = 26,
          name = "Turkey",
          type = "",
          shape = "rectangle",
          x = -32,
          y = -50.5,
          width = 32,
          height = 367,
          rotation = 0,
          visible = true,
          properties = {
            ["exitmap"] = "ThirdLevel",
            ["exitportal"] = "Farumi",
            ["script"] = "stageportal"
          }
        },
        {
          id = 27,
          name = "Salmon",
          type = "",
          shape = "rectangle",
          x = 512,
          y = -130,
          width = 32,
          height = 470.5,
          rotation = 0,
          visible = true,
          properties = {
            ["exitmap"] = "FifthLevel",
            ["exitportal"] = "Murkeye",
            ["script"] = "stageportal"
          }
        },
        {
          id = 28,
          name = "Sermi",
          type = "",
          shape = "rectangle",
          x = 0,
          y = -57.25,
          width = 32,
          height = 370.5,
          rotation = 0,
          visible = true,
          properties = {
            ["keepY"] = "true",
            ["script"] = "stageportal"
          }
        },
        {
          id = 29,
          name = "Hamwich",
          type = "",
          shape = "rectangle",
          x = 480,
          y = -139,
          width = 32,
          height = 479.25,
          rotation = 0,
          visible = true,
          properties = {
            ["keepY"] = "true",
            ["script"] = "stageportal"
          }
        },
        {
          id = 30,
          name = "",
          type = "",
          shape = "rectangle",
          x = 108,
          y = 27,
          width = 3,
          height = 19,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 31,
          name = "",
          type = "",
          shape = "rectangle",
          x = 109,
          y = 45,
          width = 16,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 32,
          name = "",
          type = "",
          shape = "rectangle",
          x = 124,
          y = 21,
          width = 3,
          height = 45,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 33,
          name = "Ultimate Cannon of Testing",
          type = "",
          shape = "rectangle",
          x = 448,
          y = 143,
          width = 32,
          height = 32,
          rotation = 0,
          visible = true,
          properties = {
            ["acceleration"] = "50",
            ["angleString"] = "180,180",
            ["pathString"] = "464,112 .. 464,208",
            ["script"] = "enemies.cannon",
            ["shotTargetString"] = "70,256 .. 385,256",
            ["speed"] = "30"
          }
        },
        {
          id = 34,
          name = "Platform1",
          type = "",
          shape = "rectangle",
          x = 433,
          y = 16,
          width = 30,
          height = 27,
          rotation = 0,
          visible = true,
          properties = {
            ["pathString"] = "365,83 .. 462, 21",
            ["script"] = "platform"
          }
        },
        {
          id = 37,
          name = "",
          type = "",
          shape = "rectangle",
          x = 385,
          y = 244.821,
          width = 12.5549,
          height = 43.9422,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 38,
          name = "",
          type = "",
          shape = "rectangle",
          x = 70,
          y = 243.68,
          width = 15.4083,
          height = 36.5234,
          rotation = 0,
          visible = true,
          properties = {}
        }
      }
    }
  }
}
