return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "0.15.2",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 16,
  height = 9,
  tilewidth = 32,
  tileheight = 32,
  nextobjectid = 37,
  properties = {
    ["entrance"] = "Giancoli",
    ["name"] = "Stage1",
    ["script"] = "defaultportals"
  },
  tilesets = {
    {
      name = "Clouds",
      firstgid = 1,
      tilewidth = 32,
      tileheight = 32,
      spacing = 1,
      margin = 1,
      image = "../../images/Clouds.png",
      imagewidth = 208,
      imageheight = 117,
      transparentcolor = "#ffaec9",
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 18,
      tiles = {}
    },
    {
      name = "tilesheet",
      firstgid = 19,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../../images/tilesheet.png",
      imagewidth = 320,
      imageheight = 160,
      transparentcolor = "#ffaec9",
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 50,
      tiles = {
        {
          id = 0,
          properties = {
            ["polygon"] = "11,0.10,1 .. 32, 0 . 11,0   .. 32, 32. 32, 0  .. 10,2. 24,16.. 24,16 . 32,32 .. 10,1 . 10,2 .. 30,28 . 32,28"
          }
        },
        {
          id = 1,
          properties = {
            ["polygon"] = "22,1.21,0 .. 21,0 . 0, 0   .. 0,0 .0, 32  ..  8,16.22,2 .. 0,32. 8,16  .. . 22,2. 22,1  .. 0,28. 2,28 ."
          }
        }
      }
    }
  },
  layers = {
    {
      type = "imagelayer",
      name = "Background",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      image = "../../images/beach background.png",
      properties = {}
    },
    {
      type = "tilelayer",
      name = "Background",
      x = 0,
      y = 0,
      width = 16,
      height = 9,
      visible = false,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        23, 23, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        23, 23, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        23, 23, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    },
    {
      type = "tilelayer",
      name = "Tile Layer 2",
      x = 0,
      y = 0,
      width = 16,
      height = 9,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {
        ["Collidable"] = "true"
      },
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 49,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 29,
        50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 29,
        23, 21, 50, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 29,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 29,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 29,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 29,
        23, 23, 30, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 29,
        23, 23, 23, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 21, 23
      }
    },
    {
      type = "objectgroup",
      name = "Entities",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      objects = {
        {
          id = 26,
          name = "Natsu",
          type = "",
          shape = "rectangle",
          x = -32,
          y = -110,
          width = 32,
          height = 376.5,
          rotation = 0,
          visible = true,
          properties = {
            ["exitmap"] = "SeventhLevel",
            ["exitportal"] = "Salamander",
            ["script"] = "stageportal"
          }
        },
        {
          id = 28,
          name = "Hermes",
          type = "",
          shape = "rectangle",
          x = 0,
          y = 123,
          width = 32,
          height = 124,
          rotation = 0,
          visible = true,
          properties = {
            ["keepY"] = "true",
            ["script"] = "stageportal"
          }
        },
        {
          id = 30,
          name = "",
          type = "",
          shape = "rectangle",
          x = 251,
          y = 37,
          width = 85,
          height = 61,
          rotation = 0,
          visible = true,
          properties = {
            ["floorlevel"] = "256",
            ["pathString"] = "139,48 .. 437,48",
            ["script"] = "bosses.ZaffreOne"
          }
        }
      }
    }
  }
}
