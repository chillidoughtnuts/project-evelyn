return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "0.15.2",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 16,
  height = 9,
  tilewidth = 32,
  tileheight = 32,
  nextobjectid = 26,
  properties = {
    ["entrance"] = "Giancoli",
    ["name"] = "Stage1",
    ["script"] = "defaultportals"
  },
  tilesets = {
    {
      name = "Clouds",
      firstgid = 1,
      tilewidth = 32,
      tileheight = 32,
      spacing = 1,
      margin = 1,
      image = "../images/Clouds.png",
      imagewidth = 208,
      imageheight = 117,
      transparentcolor = "#ffaec9",
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 18,
      tiles = {}
    },
    {
      name = "tilesheet",
      firstgid = 19,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../images/tilesheet.png",
      imagewidth = 320,
      imageheight = 160,
      transparentcolor = "#ffaec9",
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 50,
      tiles = {
        {
          id = 0,
          properties = {
            ["polygon"] = "11,0.10,1 .. 32, 0 . 11,0   .. 32, 32. 32, 0  .. 10,2. 24,16.. 24,16 . 32,32 .. 10,1 . 10,2 .. 30,28 . 32,28"
          }
        },
        {
          id = 1,
          properties = {
            ["polygon"] = "22,1.21,0 .. 21,0 . 0, 0   .. 0,0 .0, 32  ..  8,16.22,2 .. 0,32. 8,16  .. . 22,2. 22,1  .. 0,28. 2,28 ."
          }
        },
        {
          id = 37,
          properties = {
            ["polygon"] = "32,0 .. 0,0"
          }
        }
      }
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "background",
      x = 0,
      y = 0,
      width = 16,
      height = 9,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "lua",
      data = {
        28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28,
        28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28,
        28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28,
        28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28,
        28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28,
        28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28,
        28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28,
        28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28,
        28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28, 28
      }
    },
    {
      type = "tilelayer",
      name = "Tile Layer 3",
      x = 0,
      y = 0,
      width = 16,
      height = 9,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "lua",
      data = {
        7, 8, 9, 10, 11, 12, 7, 8, 9, 10, 11, 12, 7, 8, 9, 10,
        13, 14, 15, 16, 31, 32, 13, 14, 15, 16, 17, 18, 13, 14, 15, 16,
        0, 0, 0, 0, 42, 43, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 33, 42, 43, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 33, 0, 0, 46, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 56, 56, 56, 56, 0, 0, 0, 0
      }
    },
    {
      type = "tilelayer",
      name = "Tile Layer 2",
      x = 0,
      y = 0,
      width = 16,
      height = 9,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {
        ["Collidable"] = "true"
      },
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 62, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 26, 0, 0, 0, 52, 0, 0, 0, 0, 0, 0,
        0, 0, 19, 22, 21, 22, 21, 21, 21, 30, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 29, 41, 23, 23, 40, 40, 51, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 39, 23, 41, 30, 0, 0, 0, 0, 0, 26, 26, 0, 0,
        0, 0, 0, 0, 39, 23, 30, 0, 0, 0, 0, 0, 36, 36, 0, 0,
        0, 0, 0, 0, 0, 29, 23, 21, 20, 0, 0, 19, 21, 21, 20, 0
      }
    },
    {
      type = "objectgroup",
      name = "Entities",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      objects = {
        {
          id = 18,
          name = "Giancoli",
          type = "",
          shape = "rectangle",
          x = 89,
          y = 48,
          width = 61,
          height = 75,
          rotation = 0,
          visible = true,
          properties = {
            ["exitmap"] = "TestRoom",
            ["exitportal"] = "Hebden",
            ["script"] = "stageportal"
          }
        }
      }
    }
  }
}
