return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "0.15.2",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 16,
  height = 9,
  tilewidth = 32,
  tileheight = 32,
  nextobjectid = 39,
  properties = {
    ["entrance"] = "Giancoli",
    ["name"] = "Stage1",
    ["script"] = "defaultportals"
  },
  tilesets = {
    {
      name = "Clouds",
      firstgid = 1,
      tilewidth = 32,
      tileheight = 32,
      spacing = 1,
      margin = 1,
      image = "../../images/Clouds.png",
      imagewidth = 208,
      imageheight = 117,
      transparentcolor = "#ffaec9",
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 18,
      tiles = {}
    },
    {
      name = "tilesheet",
      firstgid = 19,
      tilewidth = 32,
      tileheight = 32,
      spacing = 0,
      margin = 0,
      image = "../../images/tilesheet.png",
      imagewidth = 320,
      imageheight = 160,
      transparentcolor = "#ffaec9",
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 50,
      tiles = {
        {
          id = 0,
          properties = {
            ["polygon"] = "11,0.10,1 .. 32, 0 . 11,0   .. 32, 32. 32, 0  .. 10,2. 24,16.. 24,16 . 32,32 .. 10,1 . 10,2 .. 30,28 . 32,28"
          }
        },
        {
          id = 1,
          properties = {
            ["polygon"] = "22,1.21,0 .. 21,0 . 0, 0   .. 0,0 .0, 32  ..  8,16.22,2 .. 0,32. 8,16  .. . 22,2. 22,1  .. 0,28. 2,28 ."
          }
        },
        {
          id = 37,
          properties = {
            ["polygon"] = "32,0 .. 0,0"
          }
        }
      }
    }
  },
  layers = {
    {
      type = "imagelayer",
      name = "Background",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      image = "../../images/beach background.png",
      properties = {}
    },
    {
      type = "tilelayer",
      name = "Tile Layer 3",
      x = 0,
      y = 0,
      width = 16,
      height = 9,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 16,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    },
    {
      type = "tilelayer",
      name = "Tile Layer 2",
      x = 0,
      y = 0,
      width = 16,
      height = 9,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {
        ["Collidable"] = "true"
      },
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 49, 21, 50, 0, 0, 62, 0, 0, 49, 21, 50, 0, 0, 0,
        0, 49, 23, 23, 30, 0, 0, 52, 0, 0, 29, 23, 23, 50, 0, 0,
        21, 23, 23, 23, 23, 21, 21, 23, 21, 21, 23, 23, 23, 23, 21, 21
      }
    },
    {
      type = "objectgroup",
      name = "Entities",
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      objects = {
        {
          id = 26,
          name = "Remask",
          type = "",
          shape = "rectangle",
          x = -32,
          y = -50.5,
          width = 32,
          height = 367,
          rotation = 0,
          visible = true,
          properties = {
            ["exitmap"] = "FifthLevel",
            ["exitportal"] = "Demask",
            ["script"] = "stageportal"
          }
        },
        {
          id = 27,
          name = "Popper",
          type = "",
          shape = "rectangle",
          x = 512,
          y = -26.5,
          width = 32,
          height = 367,
          rotation = 0,
          visible = true,
          properties = {
            ["exitmap"] = "SeventhLevel",
            ["exitportal"] = "Bopper",
            ["script"] = "stageportal"
          }
        },
        {
          id = 28,
          name = "Modlas",
          type = "",
          shape = "rectangle",
          x = 0,
          y = -57.25,
          width = 32,
          height = 370.5,
          rotation = 0,
          visible = true,
          properties = {
            ["keepY"] = "true",
            ["script"] = "stageportal"
          }
        },
        {
          id = 29,
          name = "Yerman",
          type = "",
          shape = "rectangle",
          x = 480,
          y = -30.25,
          width = 32,
          height = 370.5,
          rotation = 0,
          visible = true,
          properties = {
            ["keepY"] = "true",
            ["script"] = "stageportal"
          }
        },
        {
          id = 30,
          name = "",
          type = "",
          shape = "rectangle",
          x = 143,
          y = 23,
          width = 22,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 31,
          name = "",
          type = "",
          shape = "rectangle",
          x = 165,
          y = 22,
          width = 0,
          height = 11,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 32,
          name = "",
          type = "",
          shape = "rectangle",
          x = 145,
          y = 23,
          width = 0,
          height = 42,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 33,
          name = "",
          type = "",
          shape = "rectangle",
          x = 144,
          y = 65,
          width = 19,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 35,
          name = "",
          type = "",
          shape = "rectangle",
          x = 146,
          y = 43,
          width = 20,
          height = 0,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 36,
          name = "",
          type = "",
          shape = "rectangle",
          x = 166,
          y = 42,
          width = 0,
          height = 25,
          rotation = 0,
          visible = true,
          properties = {}
        },
        {
          id = 37,
          name = "Calmshell2",
          type = "",
          shape = "rectangle",
          x = 274,
          y = 232.5,
          width = 33,
          height = 26,
          rotation = 0,
          visible = true,
          properties = {
            ["durationShellString"] = "5,3,2",
            ["durationShootingString"] = "1,2,3",
            ["script"] = "enemies.calmshell"
          }
        },
        {
          id = 38,
          name = "Coral1",
          type = "",
          shape = "rectangle",
          x = 182.5,
          y = 229,
          width = 33,
          height = 26,
          rotation = 0,
          visible = true,
          properties = {
            ["script"] = "enemies.coral"
          }
        }
      }
    }
  }
}
